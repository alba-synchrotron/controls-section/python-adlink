# Control Software to interface Adlink Cards from python

## ToDo list

- [x] Search for present cards in the system
    - [x] Factory (singleton) object to scan the system looking for cards
- [ ] Initialise the control for the card 9852
    - [ ] Low level buffers
    - [x] Card configuration parameters
    - [x] Channel configuration parameters
    - [ ] Autocalibration procedure and selftest
    - [ ] Trigger
        - [x] Software trigger
    - [ ] Measurements
    - [ ] SSI
    - [ ] Other 98XX cards from the WD-DASK
        - 9820, 9816, 9826, 9846, 9842, 9848, 9814
- [ ] Other cards from D2K-DASK
- [ ] Use python-logger

## Install

Prerequisite _[Cython](https://cython.org/)_ (used 25.1) and _[python-hwinfo](https://github.com/rdobson/python-hwinfo)_ (used 0.1.7). See dependencies section.

It requires to have the sources of the _privative_ software from adlink with the kernel driver and the library _[wd-dask](https://git.cells.es/ctnda/adlink.wd-dask)_. It is hopped to have support for another Adlink sdk _[d2k-dask](https://git.cells.es/ctnda/adlink.d2k-dask)_ in a near future.

Tested using Python 3.5.3 on a 64 bits arch debian9 (stretch) machine.

```
python3 setup.py build
sudo python3 setup.py install
```

Shall be compatible with python 2.7.13 in a debian9, 64 bits also. Would be compatible with python 3.4.2 and 2.7.9 of a debian8 (jessie) on 64 bits.

## Usage

```python
>>> import adlink
>>> adlink.version()
'0.1.2-alpha'
```

A first approach, after install this module (with all its dependencies and kernel modules), should be to check the [factory](https://en.wikipedia.org/wiki/Factory_method_pattern).

```python
>>> factory = adlink.Factory()
>>> factory.nCards
 2
>>> factory.cardModels
 ['a005', '9852']
>>> factory.nCardsByModel
 {'9852': 1, 'a005': 1}
```

The information provided says that the system has _2_ cards, each from a different _model_. Then it can be requested more information about each of them:

```python
>>> infoObj = factory.getInfo('9852', 0)
>>> infoObj.bus_id
 '01:00.0'
>>> unexistingCard = factory.getInfo('9852', 1)
IndexError: No 9852 cards present with the requested index
>>> anotherInfoObj = factory.getInfo('a005', 0)
>>> anotherInfoObj.bus_id
 '03:0f.0'
```

Those objects infoObj and anotherInfoObj know the internal codification of adlink for each of the cards.

To get an object representing the physical card, the factory must be requested for such thing:

```python
>>> cardObj = factory.getCard('9852', 0)
>>> cardObj
WdDask(PCI Express 9852, 0)
>>> cardObj.cardType
51:PCI Express 9852
>>> cardObj.serialNumber
 '0x3ee9a2a5e5c5539'
>>> cardObj.FPGAVersion()
 3794015315
```

To prepare the given card to acquire, it must be configured. There are several properties for this configuration:

- Timebase: read code and assignment

**docstring**:

```python
>>> print(cardObj.timebase.__doc__)
        Code object representing the clock configuration for a card.
        Find the possible values in adlink.wdDask.timebase.tb_*
```

**Usage**:

```python
>>> cardObj.timebase
 3:[tb_IntTimeBase] Internal timer is the timebase
>>> int(cardObj.timebase)
 3
>>> str(cardObj.timebase)
 '[tb_IntTimeBase] Internal timer is the timebase'
>>> adlink.wdDask.timebase.tb_ #tab
adlink.wdDask.timebase.tb_DBoard_TimeBase    adlink.wdDask.timebase.tb_PLL_REF_PXIeCLK100
adlink.wdDask.timebase.tb_ExtTimeBase        adlink.wdDask.timebase.tb_PXI_CLK10
adlink.wdDask.timebase.tb_IntTimeBase        adlink.wdDask.timebase.tb_PXIe_CLK100
adlink.wdDask.timebase.tb_PLL_REF_EXT10      adlink.wdDask.timebase.tb_SSITimeBase
adlink.wdDask.timebase.tb_PLL_REF_PXICLK10   adlink.wdDask.timebase.tb_StarTimeBase
>>> adlink.wdDask.timebase.TimeBase(adlink.wdDask.timebase.tb_ExtTimeBase)
 0:[tb_ExtTimeBase] External timer is the timebase (NOT applicable for PCIe-9842/PXIe-9842. PXIe-9848).
>>> cardObj.timebase = adlink.wdDask.timebase.tb_ExtTimeBase
>>> cardObj.timebase
 0:[tb_ExtTimeBase] External timer is the timebase (NOT applicable for PCIe-9842/PXIe-9842. PXIe-9848)
>>> cardObj.timebase = 3
>>> cardObj.timebase
 3:[tb_IntTimeBase] Internal timer is the timebase
```

- adDutyRestore, doubleEdge, autoResetBuf: booleans

**docstring**:

```python
>>> print(cardObj.adDutyRestore.__doc__)
        Code object representing a boolean to activate/deactivate
        the AD duty cycle restore function.
>>> print(cardObj.doubleEdge?.__doc__)
        Code object representing a boolean to enable/disable
        the ping-pong mode.
>>> print(cardObj.autoResetBuf.__doc__)
        Code object representing a boolean to enable/disable
        the auto-reset mode
```

**usage**:

```python
>>> cardObj.adDutyRestore
 1:Activate AD duty cycle restore function.
>>> cardObj.adDutyRestore = 0
>>> cardObj.adDutyRestore
 0:Deactivate AD duty cycle restore function.
>>> cardObj.adDutyRestore = True
>>> cardObj.adDutyRestore
 1:Activate AD duty cycle restore function.
```

### Read input Voltage

The input voltage can be read *raw* as an integer or converted to a float by specify the Analog to Digital Range.

```python
>>> print(cardObj.analogInputRange.__doc__)
        Analog input ranges of digitaliser cards.
        Find the possible values in adlink.wdDask.adRange.ad_*
```

The constructor set (by now) a default value for the "*analog input range*":

```python
>>> cardObj.analogInputRange
 1:[AD_B_10_V] Bipolar -10V to +10V
>>> cardObj.analogInputRange = adlink.wdDask.adRange.ad_B_1_V
>>> cardObj.analogInputRange
 10:[AD_B_1_V] Bipolar -1V to +1V
```

Four diferent methods can be used then to read (and convert) the input voltage: voltageScale{,32}{,Raw}. This is to have *16* or *32* resolution and to habe the *raw* integer or the converted to a float. What is expected as the usual call is:

```python
>>> cardObj.voltageScale32
 0.0
 ```

### Channels access

**TODO**

#### Buffers management

**TODO**

#### Trigger management

Each card object gives access to a trigger representation object

```python
>>> trigger = cardObj.triggerObj
>>> trigger
 WdTrigger(card:PCI Express 9852,0; mode POST; source SOFT; trigger counts 1)
>>> trigger.mode.validValues()
 {0: ['POST', 'Post Trigger Mode'],
  1: ['PRE', 'Pre Trigger Mode'],
  2: ['MIDL', 'Middle Trigger Mode'],
  3: ['DELAY', 'Delay Trigger Mode']}
>>> trigger.source.validValues()
 {0: ['SOFT', 'Software Trigger'],
  1: ['ANALOG', 'Analog Trigger'],
  2: ['ExtD', 'External Digital Trigger'],
  3: ['SSI1', 'Trigger Signal from SSI']}
>>> trigger.triggerCounts
 1
```

While Software trigger is configure there is a method to generate the trigger that has as a parameter a numeric positive value of the trigger operation.

```python
>>> trigger.emit(0)
```

Other object attributes also looses their meanings depending on the *mode* and *source* configuration. Object representation string shows which ones have sense with the current modes and source.

### Lower level access

It is possible, by now (for development) to build an object for an specific card, bypassing the _factory_:

```python
>>> import adlink.wdDask
>>> adlink.wdDask.PCIe_9852
51
>>> adlink.wdDask.CardType(adlink.wdDask._PCIe_9852)
PCIe_9852: PCI Express 9852
>>> adlink.wdDask.WdDask?
Docstring:      Constructor class to interface Adlink cards controlled by the wd-dask.
Init docstring: Input: U16 cardType, U16 cardNum
File:           ~/src/adlink/python-adlink/adlink/wdDask/wdDask.cpython-35m-x86_64-linux-gnu.so
Type:           type
>>> adlink.wdDask.WdDask(adlink.wdDask.PCIe_9852, 0)
<adlink.wdDask.wdDask.WdDask at 0x7fa2fec77d48>
```

From the current example, there has been also saw an _adlink 2005_ in the system. But this sdk is not yet integrated, then it cannot be build the card object:

```python
>>> factory.getCard('a005', 0)
Exception: No module present for the requested card
>>> info2005 = factory.getInfo('a005', 0)
>>> info2005.cardModule
 'd2kDask'
>>> factory.submodules
 ['wd-dask']
```

## Dependencies

### Cython installation process

```
$ git clone git@github.com:cython/cython.git
$ cd cython
$ git checkout 0.25.1
$ python3 setup.py build
$ sudo python3 setup.py install
```

### hwinfo installation process

```
apt install libffi-dev
pip install python-hwinfo
```

### Privative software from Adlink

This module needs to have access to the sources of the adlink library [wd-dask](https://git.cells.es/ctnda/adlink.wd-dask). This library is subject to a "_Non Disclosure Agreement_" and its sources cannot be stored together with this repository.

There are some paths _hardcoded_ about where the include files are. They must be in "_/usr/local/include/wd-dask_" by now (or manually modify the _setup.py_). The _shared object_ (os _.so_ file) provided by this privative software shall be installed in a directory of the _$LD_LIBRARY_PATH_.

When the other adlink sdk [d2k-dask](https://git.cells.es/ctnda/adlink.d2k-dask) gets included in this module, this may be fixed.

#### Compile and install the drives and libraries

There is a [how to for the wd-dask](https://confluence.cells.es/display/CSKB/wd-dask+package+installation) in confluence, and another [how to for the d2k-dask](https://confluence.cells.es/pages/viewpage.action?spaceKey=CSKB&title=d2k-dask+package+installation)

