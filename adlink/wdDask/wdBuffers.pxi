# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

include "basicType.pxi"


cdef extern from "wd-dask.h":
    I16 WD_AI_InitialMemoryAllocated (U16 CardNumber, U32 *MemSize)
    void * WD_Buffer_Alloc (U16 wCardNumber, U32 dwSize)
    I16 WD_AI_ContBufferSetup (U16 wCardNumber, void *pwBuffer,
                               U32 dwReadCount, U16 *BufferId)
    I16 WD_AI_ContBufferReset (U16 wCardNumber)

from libc.stdlib cimport free

MAXBUFFERSALLOWED = 2
