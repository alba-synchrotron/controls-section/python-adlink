# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

from cpython cimport bool
from numpy cimport ndarray


from cardType cimport CardType
from wdChannels cimport WdChannel
from wdDask cimport WdDask

include "wdBuffers.pxi"


cdef class WdBufferFactory(object):
    cdef:
        WdDask _card
        dict _buffers
    cdef _samplesSize(self)
    cdef void* _createBuffer(self, nSamples)
    cdef __BfrRst(self)
    cdef _freeBuffer(self, WdBuffer bufferObj)

cdef class WdBuffer(object):
    cdef:
        void *_buffer
        int _nSamples
        int _bufferId
        WdBufferFactory _factory
        # bool _book
        # WdChannel _by
        ndarray _npArray
    cdef WdBuffer setBuffer(self, void *buffer, int bufId, int nSamples)
    cdef _free(self)
