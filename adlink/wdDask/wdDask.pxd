# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

from cpython cimport bool

from codeBoolean cimport ADDutyRestore, DoubleEdged, AutoResetBuf
from adRange cimport ADRange
from cardType cimport CardType
from timebase cimport TimeBase
from trigger cimport WdTrigger
from wdBuffers cimport WdBufferFactory
from wdChannels cimport WdChannels
from wdStatus cimport WdStatus

include "wdDask.pxi"


cdef class WdDask(object):
    cdef:
        CardType _cardType
        U16 _cardNum
        bool _registered
        str _serialNumber
        TimeBase _timebase
        ADDutyRestore _adDutyRestore
        DoubleEdged _doubleEdged
        AutoResetBuf _autoResetBuf
        ADRange _adRange
        dict __properties
        WdBufferFactory __bufferFactory
        WdChannels __channels
        WdTrigger _trigger
        WdStatus _status
        str __str
        str __repr
    cdef str __interpretSerialNumber(self, U8 *SerialString, U8 actualread)
