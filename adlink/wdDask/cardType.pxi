# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


cdef extern from "wd-dask.h":
    cdef int PCI_9820
    # PXI 98x6 devices
    cdef int PXI_9816D
    cdef int PXI_9826D
    cdef int PXI_9846D
    cdef int PXI_9846DW
    cdef int PXI_9816H
    cdef int PXI_9826H
    cdef int PXI_9846H
    cdef int PXI_9846HW
    cdef int PXI_9816V
    cdef int PXI_9826V
    cdef int PXI_9846V
    cdef int PXI_9846VW
    cdef int PXI_9846VID
    # PCI 98x6 devices
    cdef int PCI_9816D
    cdef int PCI_9826D
    cdef int PCI_9846D
    cdef int PCI_9846DW
    cdef int PCI_9816H
    cdef int PCI_9826H
    cdef int PCI_9846H
    cdef int PCI_9846HW
    cdef int PCI_9816V
    cdef int PCI_9826V
    cdef int PCI_9846V
    cdef int PCI_9846VW
    # PCIe 98x6 devices
    cdef int PCIe_9816D
    cdef int PCIe_9826D
    cdef int PCIe_9846D
    cdef int PCIe_9846DW
    cdef int PCIe_9816H
    cdef int PCIe_9826H
    cdef int PCIe_9846H
    cdef int PCIe_9846HW
    cdef int PCIe_9816V
    cdef int PCIe_9826V
    cdef int PCIe_9846V
    cdef int PCIe_9846VW
    # PCIe9842
    cdef int PCIe_9842
    # PXIe-9848
    cdef int PXIe_9848
    # 9852
    cdef int PCIe_9852
    cdef int PXIe_9852
    # 9814
    cdef int PCIe_9814
    # 9834
    cdef int PCIe_9834
    # obsolete
    cdef int PCI_9816
    cdef int PCI_9826
    cdef int PCI_9846


_PCI_9820   = PCI_9820
_PXI_9816D  = PXI_9816D
_PXI_9826D  = PXI_9826D
_PXI_9846D  = PXI_9846D
_PXI_9846DW = PXI_9846DW
_PXI_9816H  = PXI_9816H
_PXI_9826H  = PXI_9826H
_PXI_9846H  = PXI_9846H
_PXI_9846HW = PXI_9846HW
_PXI_9816V  = PXI_9816V
_PXI_9826V  = PXI_9826V
_PXI_9846V  = PXI_9846V
_PXI_9846VW = PXI_9846VW
_PXI_9846VID= PXI_9846VID
_PCI_9816D  = PCI_9816D
_PCI_9826D  = PCI_9826D
_PCI_9846D  = PCI_9846D
_PCI_9846DW = PCI_9846DW
_PCI_9816H  = PCI_9816H
_PCI_9826H  = PCI_9826H
_PCI_9846H  = PCI_9846H
_PCI_9846HW = PCI_9846HW
_PCI_9816V  = PCI_9816V
_PCI_9826V  = PCI_9826V
_PCI_9846V  = PCI_9846V
_PCI_9846VW = PCI_9846VW
_PCIe_9816D = PCIe_9816D
_PCIe_9826D = PCIe_9826D
_PCIe_9846D = PCIe_9846D
_PCIe_9846DW= PCIe_9846DW
_PCIe_9816H = PCIe_9816H
_PCIe_9826H = PCIe_9826H
_PCIe_9846H = PCIe_9846H
_PCIe_9846HW= PCIe_9846HW
_PCIe_9816V = PCIe_9816V
_PCIe_9826V = PCIe_9826V
_PCIe_9846V = PCIe_9846V
_PCIe_9846VW= PCIe_9846VW
_PCIe_9842  = PCIe_9842
_PXIe_9848  = PXIe_9848
_PCIe_9852  = PCIe_9852
_PXIe_9852  = PXIe_9852
_PCIe_9814  = PCIe_9814
_PCIe_9834  = PCIe_9834
_PCI_9816   = PCI_9816
_PCI_9826   = PCI_9826
_PCI_9846   = PCI_9846

