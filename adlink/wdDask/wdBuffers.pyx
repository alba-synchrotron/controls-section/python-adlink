# distutils: language=c++
# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

from cython.operator cimport dereference as deref

from libc.stdlib cimport free

import cardType
from errors cimport Error

cimport numpy
import numpy

# Great help from:
#  <http://gael-varoquaux.info/programming/ \
#   cython-example-of-exposing-c-computed-arrays-in-python-\
#   without-data-copies.html>
# Numpy must be initialised. When using numpy from C or Cython you must
# _always_ do that, or you will have segfaults
numpy.import_array()

cdef class WdBufferFactory(object):
    """
Internal object to the cython implementation. Each card (WdDask object
creation) builds its own BufferFactory.
    """
    def __cinit__(self, WdDask card):  # , int nSamples):
        self._card = card

    def __init__(self, card, *args, **kwargs):
        """
            Input: WdDask card
        """
        super(WdBufferFactory, self).__init__(*args, **kwargs)
        self._buffers = {}
        print("WdBufferFactory(%s, %d)"
              % (self._card.cardType.human, self._card.cardNum))

    def __del__(self):
        self._resetBuffers()

    def __dealloc__(self):
        # FIXME: make sure all dynamic memory is released
        self._resetBuffers()

    def __str__(self):
        return "WdBufferFactory(%s, %d)"\
            % (self._card.cardType.string, self._card.cardNum)

    def __repr__(self):
        return "WdBufferFactory(%s, %d)"\
            % (self._card.cardType.human, self._card.cardNum)

    def transferSizeKB(self):
        """
the available memory for analog input in the device driver. The continuous
analog input transfer size may not exceed this size.

Originally in KB (1024 Bytes) in the lower level library.
        """
        cdef:
            U32 memSize
        error = Error(WD_AI_InitialMemoryAllocated(self._card._cardNum,
                                                   &memSize))
        print("WD_AI_InitialMemoryAllocated(%d) -> %r"
              % (self._card._cardNum, error))
        if int(error) != 0:
            raise Exception("%r" % error)
        return memSize

    cdef _samplesSize(self):
        if self._card.cardType in [cardType._PCIe_9852, cardType._PXIe_9852]:
            return 8
        # TODO: 
        else:
            raise NotImplementedError("Not yet available for this card")

    cdef void* _createBuffer(self, nSamples):
        cdef void *buffer
        size = nSamples * self._samplesSize()
        buffer = WD_Buffer_Alloc(self._card.cardNum, size)
        print("WD_Buffer_Alloc(%d, %d)" % (self._card.cardNum, size))
        if buffer == NULL:
            raise MemoryError("WD_Buffer_Alloc(%d, %d)"
                              % (self._card._cardNum, size))
        return buffer

    def _setupBuffer(self, int nSamples):
        cdef:
            void *buffer
            U16 bufferId
        buffer = self._createBuffer(nSamples)
        error = Error(WD_AI_ContBufferSetup(self._card._cardNum,
                                            buffer, nSamples,
                                            &bufferId))
        print("WD_AI_ContBufferSetup(%d) -> %r" % (bufferId, error))
        if int(error) != 0:
            raise Exception("%r" % error)
        self._buffers[bufferId] =\
            WdBuffer(self).setBuffer(buffer, bufferId, nSamples)
        return bufferId

    @property
    def activeBuffers(self):
        return len(self._buffers)

    def keys(self):
        return self._buffers.keys()

    def __getitem__(self, id):
        if id in self._buffers:
            return self._buffers[id]
        raise KeyError("No buffer with id %d present" % (id))

    cdef __BfrRst(self):
        return Error(WD_AI_ContBufferReset (self._card._cardNum))

    def _resetBuffers(self):
        # FIXME: when __dealloc__ came here, buffers is None!
        if self._buffers is not None:
            print("** %s" % self._buffers)
            bufferIds = list(self._buffers.keys())[:]
            for bufId in bufferIds:
                print("\t** %s" % bufId)
                self._freeBuffer(self._buffers[bufId])
            self._buffers = {}
            error = self.__BfrRst()
            print("WD_AI_ContBufferReset() -> %r" % (error))
            if int(error) != 0:
                raise Exception("%r" % error)
        else:
            print("Warning: _buffers is None")

    cdef _freeBuffer(self, WdBuffer bufferObj):
        if bufferObj.id in self._buffers:
            bufId = bufferObj.id
            bufSize = bufferObj.size
            bufferObj._free()
            self._buffers.pop(bufId)
            print("free buffer %d (size %d)" % (bufId, bufSize))


cdef class WdBuffer(object):
    """
TODO: explain
    """
    def __init__(self, factory, *args, **kwargs):
        super(WdBuffer, self).__init__(*args, **kwargs)
        self._factory = factory
        #self._book = False

    def __dealloc__(self):
        self.freeBuffer()

    cdef WdBuffer setBuffer(self, void *buffer, int bufId, int nSamples):
        self._buffer = buffer
        self._bufferId = bufId
        self._nSamples = nSamples
        print("New buffer set up: id %d, samples %d" % (bufId, nSamples))
        self._prepareNumpyArrayFromBuffer()
        return self

    def _prepareNumpyArrayFromBuffer(self):
        # See: <https://docs.scipy.org/doc/numpy-1.13.0/user/\
        #       c-info.how-to-extend.html#c.PyArray_SimpleNew>
        # And: PyArray_SimpleNewFromData
        cdef:
            numpy.npy_intp shape[1]
        dimensions = 1
        shape[0] = <numpy.npy_intp> self._nSamples
        self._npArray = numpy.array(numpy.PyArray_SimpleNewFromData
                                    (dimensions, shape, numpy.NPY_DOUBLE,
                                     self._buffer), copy=False)

    cdef _free(self):
        if self._buffer != NULL:
            print("Released buffer %s" % (self._bufferId))
            free(<void*>self._buffer)
            self._buffer = NULL

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        if self._buffer == NULL:
            return "WdBuffer((%s, %d), id None, samples None)"\
                % (self._factory._card.cardType.human,
                   self._factory._card.cardNum)
        return "WdBuffer((%s, %d), id %d, samples %d)"\
            % (self._factory._card.cardType.human, self._factory._card.cardNum,
               self._bufferId, self._nSamples)

    @property
    def id(self):
        return self._bufferId

    def __array__(self):
        return self.array

    @property
    def array(self):
        return self._npArray

    @property
    def nSamples(self):
        return self._nSamples

    @property
    def size(self):
        return self._nSamples * self._factory._samplesSize()

#     @property
#     def book(self):
#         return self._book
# 
#     @property
#     def bookBy(self):
#         return self._by
# 
#     def book(self, by):
#         if not self._book:
#             self._book = True
#             self._by = by
