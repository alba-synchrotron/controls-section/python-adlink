# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2017, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


from adRange cimport ADRange
from cardType cimport CardType
from errors cimport Error



include "basicType.pxi"


cdef class WdSoftwareTriggerGenerator(object):
    """
Constructor class to wd-dask software trigger generator

>>> import adlink
>>> factory = adlink.Factory()
>>> card = factory.getCard('9852', 0)
>>> softwareTriggerObj = adlink.wdDask.WdSoftwareTriggerGenerator(card.cardNum)

To emit a trigger to the card different "operations" can be emitted to
distinguish between them.
>>> softwareTriggerObj.emit(0)

    """
    def __cinit__(self, U16 wCardNumber):  # , U8 op):
        # print("WdSoftwareTriggerGenerator().__cinit__(%d)" % (wCardNumber))
        self._wCardNumber = wCardNumber

    def __init__(self, wCardNumber, # op,
                 *args, **kwargs):
        """
            Input: U16 wCardNumbe
        """
        super(WdSoftwareTriggerGenerator, self).__init__(*args, **kwargs)
        print("WdSoftwareTriggerGenerator(%d)" % (self._wCardNumber))

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return "WdSoftwareTriggerGenerator(%d)" % (self._wCardNumber)

    def emit(self, U8 operation):
        """
            Emits a software trigger signal for the operation indicated as
            a parameter (a positive integer including the 0).
        """
        error = Error(WD_SoftTriggerGen(self._wCardNumber, operation))
        print("%r.emit(%d) -> %r" % (self, operation, error))
        if int(error) != 0:
            raise Exception(error)


cdef class WdTriggerMode(Code):
    """
Interface between human understanding of the different trigger modes and the
machine codes the lower level manages.
    """
    _validValues = {WD_AI_TRGMOD_POST: ["POST", "Post Trigger Mode"],
                    WD_AI_TRGMOD_PRE: ["PRE", "Pre Trigger Mode"],
                    WD_AI_TRGMOD_MIDL: ["MIDL", "Middle Trigger Mode"],
                    WD_AI_TRGMOD_DELAY: ["DELAY", "Delay Trigger Mode"]}

    def __init__(self, int code=WD_AI_TRGMOD_POST, *xargs, **kwargs):
        super(WdTriggerMode, self).__init__(code, *xargs, **kwargs)


cdef class WdTriggerSource(Code):
    """
Interface between human understanding of the different trigger sources that
can be used and the lower level codes the driver uses.
    """
    _validValues = {WD_AI_TRGSRC_SOFT: ["SOFT", "Software Trigger"],
                    WD_AI_TRGSRC_ANA: ["ANALOG", "Analog Trigger"],
                    WD_AI_TRGSRC_ExtD: ["ExtD", "External Digital Trigger"],
                    WD_AI_TRSRC_SSI_1: ["SSI1", "Trigger Signal from SSI"],
                    # WD_AI_TRSRC_SSI_2: ["SSI2", "Trigger..."],
                    # WD_AI_TRSRC_PXIStar: ["PXIStar",
                    #                       "Trigger from PXI_Start pin"],
                    # WD_AI_TRSRC_PXIeStart: ["PXIeStart",
                    #                         "Trigger from PXIe_Start pin"]
                    }

    def __init__(self, int code=WD_AI_TRGSRC_SOFT, *xargs, **kwargs):
        super(WdTriggerSource, self).__init__(code, *xargs, **kwargs)


cdef class WdTriggerPolarity(Code):
    """
Encapsulates the possible values of the polarity to what the trigger should
react, providing a human understandable message as well as the internal coding
the drives uses.
    """
    _validValues = {WD_AI_TrgPositive: ["Positive", "Positive edge active"],
                    WD_AI_TrgNegative: ["Negative", "Negative edge active"]}

    def __init__(self, int code=WD_AI_TrgPositive, *xargs, **kwargs):
        super(WdTriggerPolarity, self).__init__(code, *xargs, **kwargs)


cdef class WdTriggerDedicatedChannel(Code):
    """
FIXME: This encapsulation meaning is not well understood by the developer.
    """
    _validValues = {CH0ATRIG: ["Channel0", "Analog Input Channel 0"],
                    CH1ATRIG: ["Channel1", "Analog Input Channel 1"],
                    CH2ATRIG: ["Channel2", "Analog Input Channel 2"],
                    CH3ATRIG: ["Channel3", "Analog Input Channel 3"],
                    CH4ATRIG: ["Channel4", "Analog Input Channel 4"],
                    CH5ATRIG: ["Channel5", "Analog Input Channel 5"],
                    CH6ATRIG: ["Channel6", "Analog Input Channel 6"],
                    CH7ATRIG: ["Channel7", "Analog Input Channel 7"]}

    def __init__(self, int code=CH0ATRIG, *xargs, **kwargs):
        super(WdTriggerDedicatedChannel, self).__init__(code, *xargs, **kwargs)


cdef class WdTriggerLevel(object):
    """
Analog trigger level setting or digital trigger threshold. The valid setpoint
depends, on analog behaviour, in the ADrange (but not implemented the
restriction) as well as, in digital behaviour a 0 is a threshold for 3.3V
trigger.
FIXME: This parameter is not well understood by the developer.
    """
    # FIXME: The valid range depends on the AI range. That's why the object
    #        is propagated here.
    #        5 configurations are shown in the documentation:
    #        * AD_B_5_V => value between [-5, 5]
    #        * AD_B_1_V => value between [-1, 1]
    #        * AD_B_10_V => value between [-10, 10]
    #        * AD_B_2_V => value between [-2, 2]
    #        * AD_B_0_2_V => value between [-0.2, 0.2]
    # FIXME: when implemented, a change on the range object may be reported
    #        here.
    # TODO: seems to also needs a relation with the WdTriggerSource
    def __init__(self, double value=0.0, ADRange range=None, *xargs, **kwargs):
        super(WdTriggerLevel, self).__init__(*xargs, **kwargs)
        self._value = value
        self._range = range

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return "%g" % self._value

    def __float__(self):
        return self._value

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        # TODO: check if the ADRange allows this setting
        self._value = float(value)

    @property
    def range(self):
        return self._range


cdef class WdTrigger(object):
    """
Constructor class to wd-dask trigger. The object represents the trigger for a
given card. It provides a set of attributes that depends on the mode and
source if they have an active meaning.
    """
    def __cinit__(self, CardType cardType, U16 wCardNumber, ADRange range):
        print("WdTrigger().__cinit__()")
        self._cardType = cardType
        self._wCardNumber = wCardNumber
        self._swTrigger = WdSoftwareTriggerGenerator(self._wCardNumber)
        self._mode = WdTriggerMode()
        self._source = WdTriggerSource()
        self._polarity = WdTriggerPolarity()
        self._channel = WdTriggerDedicatedChannel()  # TODO: what does it means?
        self._level = WdTriggerLevel(range=range)
        self._preTriggerSamples = 1
        self._postTriggerSamples = 1
        self._delay = 0
        self._triggerCounts = 1
        self.__configureTrigger()

    def __init__(self, cardType, wCardNumber, range, *args, **kwargs):
        """
            Input: 
        """
        super(WdTrigger, self).__init__(*args, **kwargs)
        self.__str = "WdTrigger(%s, %d)" % (self._cardType.human,
                                            self._wCardNumber)
        print("WdTrigger(%d).__init__()" % (self._wCardNumber))

    def __str__(self):
        return self.__str

    def __repr__(self):
        return self.__repr

    def __configureTrigger(self):
        error = Error(WD_AI_Trig_Config(self._wCardNumber, self._mode,
                                        self._source, self._polarity,
                                        self._channel, self._level,
                                        self._preTriggerSamples,
                                        self._postTriggerSamples,
                                        self._delay, self._triggerCounts))
        self.__buildRepr__()
    
    def __buildRepr__(self):
        repr = ["WdTrigger(card:%s,%d; " % (self._cardType.human,
                                            self._wCardNumber)]
        repr += ["mode %s; " % (self._mode.string)]
        repr += ["source %s; " % (self._source.string)]
        if self._source.code not in [WD_AI_TRGSRC_SOFT]:
            repr += ["polarity %s; " % (self._polarity.string)]
        # TODO: channel and level hasn't included because they are not
        #       yet well understood.
        if self._mode.code in [WD_AI_TRGMOD_MIDL]:
            repr += ["samples pre %d, post %d; " % (self._preTriggerSamples,
                                                 self._postTriggerSamples)]
        if self._mode.code in [WD_AI_TRGMOD_DELAY]:
            repr += ["delay %d" % self._delay]
        repr += ["trigger counts %d)" % self._triggerCounts]
        #repr[-1] = repr[-1][:-2] + ")" # remove the last "; " and close
        self.__repr = ''.join(repr)

    @property
    def softwareTriggerObj(self):
        return self._swTrigger

    def emit(self, U8 operation):
        if self._source.code in [WD_AI_TRGSRC_SOFT]:
            self.softwareTriggerObj.emit(operation)
        else:
            raise AssertionError("Only in Software trigger mode")

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, value):
        if value in self._mode.validValues():
            self._mode.code = value
            self.__configureTrigger()
        else:
            raise ValueError("Invalid mode (%s), check validValues()"
                             % (value))

    @property
    def source(self):
        return self._source

    @source.setter
    def source(self, value):
        if value in self._source.validValues():
            self._source.code = value
            self.__configureTrigger()
        else:
            raise ValueError("Invalid source (%s), check validValues()"
                             % (value))

    @property
    def polarity(self):
        return self._polarity

    @polarity.setter
    def polarity(self, value):
        if value in self._polarity.validValues():
            self._polarity.code = value
            self.__configureTrigger()
        else:
            raise ValueError("Invalid polarity (%s), check validValues()"
                             % (value))

    @property
    def channel(self):
        return self._channel

    @property
    def level(self):
        return self._level

    @property
    def preTriggerSamples(self):
        return self._preTriggerSamples

    @preTriggerSamples.setter
    def preTriggerSamples(self, value):
        if isinstance(value, int):
            if 0 <= value <= 0x7fffffff:
                self._preTriggerSamples = value
                self.__configureTrigger()
            else:
                raise ValueError("Out of range (%d)" % (value))
        else:
            raise TypeError("Positive integer required")

    @property
    def postTriggerSamples(self):
        return self._postTriggerSamples

    @postTriggerSamples.setter
    def postTriggerSamples(self, value):
        if isinstance(value, int):
            if 0 <= value <= 0x7fffffff:
                self._postTriggerSamples = value
                self.__configureTrigger()
            else:
                raise ValueError("Out of range (%d)" % (value))
        else:
            raise TypeError("Positive integer required")

    @property
    def delay(self):
        return self._delay

    @delay.setter
    def delay(self, value):
        if isinstance(value, int):
            if 0 <= value <= 0x7fffffff:
                self._delay = value
                self.__configureTrigger()
            else:
                raise ValueError("Out of range (%d)" % (value))
        else:
            raise TypeError("Positive integer required")

    @property
    def triggerCounts(self):
        return self._triggerCounts

    @triggerCounts.setter
    def triggerCounts(self, value):
        if isinstance(value, int):
            if 0 <= value <= 0x7fffffff:
                # TODO: If average mode is enabled in PCIe/PXIe-9852 cards
                #       upper limit is 0xffff
                self._triggerCounts = value
                self.__configureTrigger()
            else:
                raise ValueError("Out of range (%d)" % (value))
        else:
            raise TypeError("Positive integer required")
