# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


from adRange cimport ADRange
from chanParams cimport ChannelParameter
from errors cimport Error
from wdBuffers cimport WdBufferFactory, MAXBUFFERSALLOWED
from wdDask cimport WdDask

from libc.stdlib cimport malloc, free


include "chanParams.pxi"


cdef class WdChannel(object):
    """
Constructor class to access a channel of a card.

This object represents a single channel from a given card: 
>>> import adlink
>>> factory = adlink.Factory()
>>> card = factory.getCard('9852', 0)
>>> ch0 = card.channel(0)
>>> ch0.parameters
 {'adc_bandwidth': '0:[BANDWIDTH_DEVICE_DEFAULT] Default',
  'adc_dither': '0:[ADC_DITHER_DIS] ADC Dither Disabled',
  'ai_coupling': '0:[DC_Coupling] DC',
   'impedance': '0:[IMPEDANCE_50Ohm] 50 Ohm',
  'signal_fir': '0:[FIR_DIR] Signal FIR disable'}

    """
    def __cinit__(self, WdDask card, int channel):
        # print("WdChannel().__cinit__(%s, %d)" % (card, channel))
        self._card = card
        self._channel = channel
        self._adRange = ADRange()
        self._impedance = ChannelParameter("AI_IMPEDANCE", _IMPEDANCE_50Ohm)
        self._adc_dither = ChannelParameter("ADC_DITHER", _ADC_DITHER_DIS)
        self._ai_coupling = ChannelParameter("AI_COUPLING", _DC_Coupling)
        self._adc_bandwidth = ChannelParameter("ADC_BANDWIDTH",
                                               _BANDWIDTH_DEVICE_DEFAULT)
        self._signal_fir = ChannelParameter("SIGNAL_FIR", _FIR_DIS)
        self._parameters = {'impedance': "%r" % self._impedance.code,
                            'adc_dither': "%r" % self._adc_dither.code,
                            'ai_coupling': "%r" % self._ai_coupling.code,
                            'adc_bandwidth': "%r" % self._adc_bandwidth.code,
                            'signal_fir': "%r" % self._signal_fir.code}

    def __init__(self, card, channel, *args, **kwargs):
        """
            Input: WdDask channel
        """
        super(WdChannel, self).__init__(*args, **kwargs)
        print("WdChannel(card %s, channel %d)"
              % (self._card, self._channel))

    def __str__(self):
        return "WdChannel((%s, %d), %d)" % (self._card.cardType.string,
                                      self._card.cardNum,
                                      self._channel)

    def __repr__(self):
        return "WdChannel((%s, %d), %d)" % (self._card.cardType.human,
                                      self._card.cardNum,
                                      self._channel)

    @property
    def number(self):
        return self._channel

    @property
    def card(self):
        return self._card

    @property
    def analogInputRange(self):
        return self._adRange

    @analogInputRange.setter
    def analogInputRange(self, value):
        self._rangeConfig(self._adRange.code)
        self._adRange.code = value

    def _rangeConfig(self, U16 wAdRange):
        error = Error(WD_AI_CH_Config(self._card._cardNum, self._channel,
                                      wAdRange))
        print("WD_AI_CH_Config() -> %r" % (error))
        if int(error) != 0:
            raise Exception("%r" % error)

    def _rangeConfig(self, ADRange wAdRange):
        self.rangeConfig(int(wAdRange))
        # error = Error(WD_AI_CH_Config(self._card._cardNum, self._channel,
        #                               int(wAdRange)))
        # print("WD_AI_CH_Config() -> %r" % (error))
        # if int(error) != 0:
        #     raise Exception("%r" % error)

    @property
    def parameters(self):
        return self._parameters

    @property
    def impedance(self):
        return self._impedance.code

    @impedance.setter
    def impedance(self, value):
        self._impedance.value = value
        self._changeParam(self._impedance)
        self._parameters["impedance"] = self._impedance.code

    @property
    def _impedanceObj(self):
        return self._impedance

    @property
    def adc_dither(self):
        return self._adc_dither.code

    @adc_dither.setter
    def adc_dither(self, value):
        self._adc_dither.value = value
        self._changeParam(self.adc_dither)
        self._parameters["adc_dither"] = self._adc_dither.code

    @property
    def _adc_ditherObj(self):
        return self._adc_dither

    @property
    def ai_coupling(self):
        return self._ai_coupling.code

    @ai_coupling.setter
    def ai_coupling(self, value):
        self._ai_coupling.value = value
        self._changeParam(self.ai_coupling)
        self._parameters["ai_coupling"] = self._ai_coupling.code

    @property
    def _ai_couplingObj(self):
        return self._ai_coupling

    @property
    def adc_bandwidth(self):
        return self._adc_bandwidth.code

    @adc_bandwidth.setter
    def adc_bandwidth(self, value):
        self._adc_bandwidth.value = value
        self._changeParam(self.adc_bandwidth)
        self._parameters["adc_bandwidth"] = self._adc_bandwidth.code

    @property
    def _adc_bandwidthObj(self):
        return self._adc_bandwidth

    @property
    def signal_fir(self):
        return self._signal_fir.code

    @signal_fir.setter
    def signal_fir(self, value):
        self._signal_fir.value = value
        self._changeParam(self.signal_fir)
        self._parameters["signal_fir"] = self._signal_fir.code

    @property
    def _signal_firObj(self):
        return self._signal_fir

    def _changeParam(self, ChannelParameter paramObj):
        error = Error(WD_AI_CH_ChangeParam(self._card._cardNum, self._channel,
                                           paramObj.parameter, paramObj.value))
        print("WD_AI_CH_ChangeParam() -> %r" % (error))
        if int(error) != 0:
            raise Exception("%r" % error)

    @property
    def buffer(self):
        return self._buffer

    def __requestNewBuffer(self, nSamples):
        factory = self._card._bufferFactoryObj
        idx = factory._setupBuffer(nSamples)
        return factory[idx]

    def _preRead(self, nSamples):
        if self._buffer is None:
            self._buffer = self.__requestNewBuffer(nSamples)
        else:  # if self._buffer is not None:
            # When nSamples doesn't correspond with previous reads
            if self._buffer.nSamples != nSamples:
                self._card._bufferFactoryObj._resetBuffers()
                self._buffer = self.__requestNewBuffer(nSamples)
        # TODO: what is previous acquisition was multiChannel?

    def read(self, nSamples):
        cdef:
            U32 scanIntrv
            U32 SampIntrv
            U16 SyncMode
        scanIntrv = 1
        sampIntrv = 1
        syncMode = SYNCH_OP
        self._preRead(nSamples)
        error = Error(WD_AI_ContReadChannel(self._card._cardNum, self._channel,
                                            self._buffer.id, nSamples,
                                            scanIntrv, sampIntrv, syncMode))
        print("WD_AI_ContReadChannel() -> %r" % (error))
        if int(error) != 0:
            raise Exception("%r" % error)
        return self._buffer.array


cdef class WdChannels(object):
    """
Constructor class to access all the channels of an specific card.

This object represents the hole set of channels from a given card: 
>>> import adlink
>>> factory = adlink.Factory()
>>> card = factory.getCard('9852', 0)
>>> card.nChannels
 2
>>> card.channels.parameters
{'adc_bandwidth': 0:[BANDWIDTH_DEVICE_DEFAULT] Default,
 'adc_dither': 0:[ADC_DITHER_DIS] ADC Dither Disabled,
 'ai_coupling': 0:[DC_Coupling] DC,
 'impedance': [1:[IMPEDANCE_HI] 1M Ohm, 0:[IMPEDANCE_50Ohm] 50 Ohm],
 'signal_fir': 0:[FIR_DIR] Signal FIR disable}

When only one element is returned means that all the channels have the same
configuration:
>>> card.channels.ai_coupling
 0:[DC_Coupling] DC

But when it is a list, it contains the configuration of each of the available
channels in a sorted list:
>>> card.channels.impedance
 [1:[IMPEDANCE_HI] 1M Ohm, 0:[IMPEDANCE_50Ohm] 50 Ohm]

    """
    def __init__(self, *args, **kwargs):
        super(WdChannels, self).__init__(*args, **kwargs)
        print("WdChannels()")
        self._card = None
        self._channels = {}
        self._impedance = ChannelParameter("AI_IMPEDANCE", _IMPEDANCE_50Ohm)
        self._adc_dither = ChannelParameter("ADC_DITHER", _ADC_DITHER_DIS)
        self._ai_coupling = ChannelParameter("AI_COUPLING", _DC_Coupling)
        self._adc_bandwidth = ChannelParameter("ADC_BANDWIDTH",
                                               _BANDWIDTH_DEVICE_DEFAULT)
        self._signal_fir = ChannelParameter("SIGNAL_FIR", _FIR_DIS)
        self._parameters = {'impedance': None,
                            'adc_dither': None,
                            'ai_coupling': None,
                            'adc_bandwidth': None,
                            'signal_fir': None}

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return "WdChannels(%s)[%d]" % (self._card, len(self._channels.keys()))

    @property
    def parameters(self):
        self._buildParametersDict()
        return self._parameters

    def _buildParametersDict(self):
        self._parameters['impedance'] = self.impedance
        self._parameters['adc_dither'] = self.adc_dither
        self._parameters['ai_coupling'] = self.ai_coupling
        self._parameters['adc_bandwidth'] = self.adc_bandwidth
        self._parameters['signal_fir'] = self.signal_fir

    def _refreshCodes(self, channelParameter, name):
        allTheSame = True
        collectedCodes = []
        codesUsed = []
        # print("From common %r code, refresh" % (channelParameter.code))
        for n in self._channels:
            ch = self._channels[n]
            chAttr = getattr(ch, name)
            if channelParameter.code != chAttr:
                # print("channel %d is different: %r" % (n, chAttr))
                allTheSame = False
            collectedCodes.append(chAttr)
            if chAttr not in codesUsed:
                codesUsed.append(chAttr)
        if allTheSame:
            return channelParameter.code
        if len(codesUsed) == 1:
            # print("All have change to %r" % chAttr)
            return chAttr
        return collectedCodes

    def _applyCode(self, channelParameter, name, value):
        channelParameter.value = value
        self._changeParam(channelParameter)
        self._parameters[name] = channelParameter.code
        for n in self._channels:
            ch = self._channels[n]
            chAttr = getattr(ch, name)
            chAttr.code = channelParameter.code
            ch.parameters[name] = chAttr.code

    @property
    def impedance(self):
        return self._refreshCodes(self._impedance, 'impedance')

    @impedance.setter
    def impedance(self, value):
        self._applyCode(self._impedance, "impedance", value)

    @property
    def adc_dither(self):
        return self._refreshCodes(self._adc_dither, 'adc_dither')

    @adc_dither.setter
    def adc_dither(self, value):
        self._applyCode(self._adc_dither, "adc_dither", value)

    @property
    def ai_coupling(self):
        return self._refreshCodes(self._ai_coupling, 'ai_coupling')

    @ai_coupling.setter
    def ai_coupling(self, value):
        self._applyCode(self._ai_coupling, "ai_coupling", value)

    @property
    def adc_bandwidth(self):
        return self._refreshCodes(self._adc_bandwidth, 'adc_bandwidth')

    @adc_bandwidth.setter
    def adc_bandwidth(self, value):
        self._applyCode(self._adc_bandwidth, "adc_bandwidth", value)

    @property
    def signal_fir(self):
        return self._refreshCodes(self._signal_fir, 'signal_fir')

    @signal_fir.setter
    def signal_fir(self, value):
        self._applyCode(self._signal_fir, "signal_fir", value)

    def _changeParam(self, ChannelParameter paramObj):
        error = Error(WD_AI_CH_ChangeParam(self._card._cardNum, -1,
                                           paramObj.parameter, paramObj.value))
        print("WD_AI_CH_ChangeParam() -> %r" % (error))
        if int(error) != 0:
            raise Exception("%r" % error)

    ##########################
    # Dictionary behaviour ---
    def __keytransform__(self, key):
        try:
            key = int(key)
            if key < 0:
                raise KeyError("Key must be positive")
        except:
            raise KeyError("Invalid key %r" % (str(key)))
        return key

    def __getitem__(self, key):
        key = self.__keytransform__(key)
        if key not in self._channels:
            raise KeyError("Card %s doesn't have channel %d"
                           % (self._card, key))
        return self._channels[key]

    def __setitem__(self, key, value):
        if value.__class__.__name__ not in ['WdChannel']:
            raise TypeError("Only WdChannel() items are allowed")
        if value.number != key:
            raise IndexError("key %d doesn't correspond with channel number %d"
                             % (key, value.number))
        if self._card is None:
            self._card = value.card
        elif self._card != value.card:
            raise AssertionError("All cards grouped must be from the same "
                                 "card (%r, %r)" % (self._card, value.card))
        # print("added new channel %d: %s" % (key, value))
        self._channels[key] = value

    def __contains__(self, key):
        return self._channels.__contains__(self.__keytransform__(key))

    @property
    def buffer(self):
        return self._buffer

    def __requestNewBuffer(self, nSamples):
        factory = self._card._bufferFactoryObj
        idx = factory._setupBuffer(nSamples)
        return factory[idx]

    def _preReadMultiChannels(self, nSamples):
        if self._buffer is None:
            self._buffer = self.__requestNewBuffer(nSamples)
        else:  # if self._buffer is not None:
            # When nSamples doesn't correspond with previous reads
            if self._buffer.nSamples != nSamples:
                self._card._bufferFactoryObj._resetBuffers()
                self._buffer = self.__requestNewBuffer(nSamples)
        # TODO: what is previous acquisition was single channels and one or
        #       more of the channels has its own buffer.

    def readMultiChannels(self, nSamples):
        # FIXME: this seems to save both acquisitions in one single buffer
        #        but how it was made returns two buffers. One with, perhaps,
        #        both channels with a certain merge, and the other with all
        #        zeros.
        cdef:
            U16 numChans
            U16 *chans2read
            U32 scanIntrv
            U32 SampIntrv
            U16 syncMode
        numChans = len(self._channels)
        chans2read = <U16*>malloc(numChans*sizeof(U16))
        arrays = []
        for i in range(numChans):
            chans2read[i] = i
        self._preReadMultiChannels(nSamples)
        scanIntrv = 1
        sampIntrv = 1
        syncMode = SYNCH_OP
        error = Error(WD_AI_ContReadMultiChannels(self._card._cardNum,
                                                  numChans, chans2read,
                                                  self._buffer.id,
                                                  nSamples/numChans,
                                                  scanIntrv, sampIntrv,
                                                  syncMode))
        free(chans2read)
        print("WD_AI_ContReadMultiChannels() -> %r" % (error))
        if int(error) != 0:
            raise Exception("%r" % error)
        return self._buffer.array
