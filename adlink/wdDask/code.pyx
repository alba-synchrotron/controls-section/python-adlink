# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


include "code.pxi"


cdef class Code(object):
    """
Abstract superclass to interface the adlink codes and provide a human string
that can understand the meaning.
    """
    def __init__(self, int code):
        self._setValue(code)

    def __int__(self):
        return self._code

    def __str__(self):
        return self.meaning

    def __repr__(self):
        return "%d:%s" % (self._code, self.meaning)
        # ??: return "%d:%s" % (self._interpret(self._code))

    def __richcmp__(self, other, int op):
        if op in [2, 3]:  # ==, !=
            if int(self) == int(other):
                return True if op == 2 else False
            return False if op == 2 else True
        return False

    @property
    def code(self):
        # return self._interpret(self._code)[0]
        return self._code

    @code.setter
    def code(self, value):
        self._setValue(value)

    def _setValue(self, value):
        if value in self.validCodes():
            self._code = value
#         elif value in self.validStrings():
#             code = self.validStrings().index(value)
#             self._code = code
        else:
            raise ValueError("Invalid value to assign")

    @property
    def meaning(self):
        strCode, strHelp = self._interpret(self._code)
        return "[%s] %s" % (strCode, strHelp)

    @property
    def string(self):
        strCode, _ = self._interpret(self._code)
        return "%s" % (strCode)

    @property
    def human(self):
        _, strHelp = self._interpret(self._code)
        return "%s" % (strHelp)

    def _interpret(self, value):
        """
Method to be implemented on the subclasses and must return an array with two
elements: an integer with the code and a string with the meaning.
        """
        if not hasattr(self, "_validValues"):
            raise NotImplementedError("Not implemented in abstract "
                                      "superclass!")
        return self._validValues.get(value, [None, "Unknown %s (%d)"
                                             % (self.__class__.__name__,
                                                self._code)])

    # FIXME: _interpret is called each time the get is called. For better
    #        performance it must be called only when a code is set.
    #        but the implementation in the subclass may require a call
    #        each time.

    def validValues(self):
        """
Method to be implemented on the subclasses and must return a dictionary with
codes in the keys and each item a list with the code as string in the first
element, followed by the meaning.
            
        """
        if not hasattr(self, "_validValues"):
            raise NotImplementedError("Not implemented in abstract "
                                      "superclass!")
        return self._validValues

    def validCodes(self):
        return list(self._validValues.keys())

    def validStrings(self):
        if self._validStrings is None:
            self._validStrings = []
            for code in self._validValues.keys():
                self._validStrings.append(self._validValues[code][0])
        return self._validStrings[:]


cdef class Impedance(Code):
    _validValues = {_IMPEDANCE_50Ohm: ["IMPEDANCE_50Ohm", "50 Ohm"],
                    _IMPEDANCE_HI: ["IMPEDANCE_HI", "1M Ohm"]}


cdef class AdcDither(Code):
    _validValues = {_ADC_DITHER_DIS: ["ADC_DITHER_DIS", "ADC Dither Disabled"],
                    _ADC_DITHER_EN: ["ADC_DITHER_EN", "ADC Dither Enabled"]}


cdef class Coupling(Code):
    _validValues = {_DC_Coupling: ["DC_Coupling", "DC"],
                    _AC_Coupling: ["AC_Coupling", "AC"]}


cdef class Bandwidth(Code):
    _validValues = {_BANDWIDTH_DEVICE_DEFAULT: ["BANDWIDTH_DEVICE_DEFAULT",
                                                "Default"],
                    _BANDWIDTH_20M: ["BANDWIDTH_20M", "20M"],
                    _BANDWIDTH_100M: ["BANDWIDTH_100M", "100M"]}


cdef class SignalFir(Code):
    _validValues = {_FIR_DIS: ["FIR_DIR", "Signal FIR disable"],
                    _FIR_EN_20M: ["FIR_EN_20M", "Signal FIR 20M"],
                    _FIR_EN_10M: ["FIR_EN_10M", "Signal FIR 10M"]}
