# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


cdef extern from "wd-dask.h":
    cdef int NoError
    cdef int ErrorUnknownCardType
    cdef int ErrorInvalidCardNumber
    cdef int ErrorTooManyCardRegistered
    cdef int ErrorCardNotRegistered
    cdef int ErrorFuncNotSupport
    cdef int ErrorInvalidIoChannel
    cdef int ErrorInvalidAdRange
    cdef int ErrorContIoNotAllowed
    cdef int ErrorDiffRangeNotSupport
    cdef int ErrorLastChannelNotZero
    cdef int ErrorChannelNotDescending
    cdef int ErrorChannelNotAscending
    cdef int ErrorOpenDriverFailed
    cdef int ErrorOpenEventFailed
    cdef int ErrorTransferCountTooLarge
    cdef int ErrorNotDoubleBufferMode
    cdef int ErrorInvalidSampleRate
    cdef int ErrorInvalidCounterMode
    cdef int ErrorInvalidCounter
    cdef int ErrorInvalidCounterState
    cdef int ErrorInvalidBinBcdParam
    cdef int ErrorBadCardType
    cdef int ErrorInvalidDaRefVoltage
    cdef int ErrorAdTimeOut
    cdef int ErrorNoAsyncAI
    cdef int ErrorNoAsyncAO
    cdef int ErrorNoAsyncDI
    cdef int ErrorNoAsyncDO
    cdef int ErrorNotInputPort
    cdef int ErrorNotOutputPort
    cdef int ErrorInvalidDioPort
    cdef int ErrorInvalidDioLine
    cdef int ErrorContIoActive
    cdef int ErrorDblBufModeNotAllowed
    cdef int ErrorConfigFailed
    cdef int ErrorInvalidPortDirection
    cdef int ErrorBeginThreadError
    cdef int ErrorInvalidPortWidth
    cdef int ErrorInvalidCtrSource
    cdef int ErrorOpenFile
    cdef int ErrorAllocateMemory
    cdef int ErrorDaVoltageOutOfRange
    cdef int ErrorInvalidSyncMode
    cdef int ErrorInvalidBufferID
    cdef int ErrorInvalidCNTInterval
    cdef int ErrorReTrigModeNotAllowed
    cdef int ErrorResetBufferNotAllowed
    cdef int ErrorAnaTrggerLevel
    cdef int ErrorDAQEvent
    cdef int ErrorInvalidDataSize
    cdef int ErrorOffsetCalibration
    cdef int ErrorGainCalibration
    cdef int ErrorCountOutofSDRAMSize
    cdef int ErrorNotStartTriggerModule
    cdef int ErrorInvalidRouteLine
    cdef int ErrorInvalidSignalCode
    cdef int ErrorInvalidSignalDirection
    cdef int ErrorTRGOSCalibration
    cdef int ErrorNoSDRAM
    cdef int ErrorIntegrationGain
    cdef int ErrorAcquisitionTiming
    cdef int ErrorIntegrationTiming
    cdef int ErrorInvalidTraceCnt
    cdef int ErrorTriggerSource
    cdef int ErrorInvalidTimeBase
    cdef int ErrorUndefinedParameter
    cdef int ErrorInvalidParameter
    cdef int ErrorNotDAQSteppedMode
    cdef int ErrorBufAddrNotQuadDWordAlignment
    cdef int ErrorSignalSetup
    cdef int ErrorDeviceIsRunning
    cdef int ErrorPLLNotLocked
    cdef int ErrorCLKNotPresent
    cdef int ErrorDDRInitFailed
    cdef int ErrorSelfTestFailed
    cdef int ErrorCalAddress
    cdef int ErrorInvalidCalBank
    cdef int ErrorLockMemory
    cdef int ErrorInvalidMemBank
    cdef int ErrorInvalidCALSource
    cdef int ErrorInvalidDAPolar
    cdef int ErrorConfigIoctl
    cdef int ErrorAsyncSetIoctl
    cdef int ErrorDBSetIoctl
    cdef int ErrorDBHalfReadyIoctl
    cdef int ErrorContOPIoctl
    cdef int ErrorContStatusIoctl
    cdef int ErrorPIOIoctl
    cdef int ErrorDIntSetIoctl
    cdef int ErrorWaitEvtIoctl
    cdef int ErrorOpenEvtIoctl
    cdef int ErrorCOSIntSetIoctl
    cdef int ErrorMemMapIoctl
    cdef int ErrorMemUMapSetIoctl
    cdef int ErrorCTRIoctl
    cdef int ErrorGetResIoctl

_NoError                           = NoError
_ErrorUnknownCardType              = ErrorUnknownCardType
_ErrorInvalidCardNumber            = ErrorInvalidCardNumber
_ErrorTooManyCardRegistered        = ErrorTooManyCardRegistered
_ErrorCardNotRegistered            = ErrorCardNotRegistered
_ErrorFuncNotSupport               = ErrorFuncNotSupport
_ErrorInvalidIoChannel             = ErrorInvalidIoChannel
_ErrorInvalidAdRange               = ErrorInvalidAdRange
_ErrorContIoNotAllowed             = ErrorContIoNotAllowed
_ErrorDiffRangeNotSupport          = ErrorDiffRangeNotSupport
_ErrorLastChannelNotZero           = ErrorLastChannelNotZero
_ErrorChannelNotDescending         = ErrorChannelNotDescending
_ErrorChannelNotAscending          = ErrorChannelNotAscending
_ErrorOpenDriverFailed             = ErrorOpenDriverFailed
_ErrorOpenEventFailed              = ErrorOpenEventFailed
_ErrorTransferCountTooLarge        = ErrorTransferCountTooLarge
_ErrorNotDoubleBufferMode          = ErrorNotDoubleBufferMode
_ErrorInvalidSampleRate            = ErrorInvalidSampleRate
_ErrorInvalidCounterMode           = ErrorInvalidCounterMode
_ErrorInvalidCounter               = ErrorInvalidCounter
_ErrorInvalidCounterState          = ErrorInvalidCounterState
_ErrorInvalidBinBcdParam           = ErrorInvalidBinBcdParam
_ErrorBadCardType                  = ErrorBadCardType
_ErrorInvalidDaRefVoltage          = ErrorInvalidDaRefVoltage
_ErrorAdTimeOut                    = ErrorAdTimeOut
_ErrorNoAsyncAI                    = ErrorNoAsyncAI
_ErrorNoAsyncAO                    = ErrorNoAsyncAO
_ErrorNoAsyncDI                    = ErrorNoAsyncDI
_ErrorNoAsyncDO                    = ErrorNoAsyncDO
_ErrorNotInputPort                 = ErrorNotInputPort
_ErrorNotOutputPort                = ErrorNotOutputPort
_ErrorInvalidDioPort               = ErrorInvalidDioPort
_ErrorInvalidDioLine               = ErrorInvalidDioLine
_ErrorContIoActive                 = ErrorContIoActive
_ErrorDblBufModeNotAllowed         = ErrorDblBufModeNotAllowed
_ErrorConfigFailed                 = ErrorConfigFailed
_ErrorInvalidPortDirection         = ErrorInvalidPortDirection
_ErrorBeginThreadError             = ErrorBeginThreadError
_ErrorInvalidPortWidth             = ErrorInvalidPortWidth
_ErrorInvalidCtrSource             = ErrorInvalidCtrSource
_ErrorOpenFile                     = ErrorOpenFile
_ErrorAllocateMemory               = ErrorAllocateMemory
_ErrorDaVoltageOutOfRange          = ErrorDaVoltageOutOfRange
_ErrorInvalidSyncMode              = ErrorInvalidSyncMode
_ErrorInvalidBufferID              = ErrorInvalidBufferID
_ErrorInvalidCNTInterval           = ErrorInvalidCNTInterval
_ErrorReTrigModeNotAllowed         = ErrorReTrigModeNotAllowed
_ErrorResetBufferNotAllowed        = ErrorResetBufferNotAllowed
_ErrorAnaTrggerLevel               = ErrorAnaTrggerLevel
_ErrorDAQEvent                     = ErrorDAQEvent
_ErrorInvalidDataSize              = ErrorInvalidDataSize
_ErrorOffsetCalibration            = ErrorOffsetCalibration
_ErrorGainCalibration              = ErrorGainCalibration
_ErrorCountOutofSDRAMSize          = ErrorCountOutofSDRAMSize
_ErrorNotStartTriggerModule        = ErrorNotStartTriggerModule
_ErrorInvalidRouteLine             = ErrorInvalidRouteLine
_ErrorInvalidSignalCode            = ErrorInvalidSignalCode
_ErrorInvalidSignalDirection       = ErrorInvalidSignalDirection
_ErrorTRGOSCalibration             = ErrorTRGOSCalibration
_ErrorNoSDRAM                      = ErrorNoSDRAM
_ErrorIntegrationGain              = ErrorIntegrationGain
_ErrorAcquisitionTiming            = ErrorAcquisitionTiming
_ErrorIntegrationTiming            = ErrorIntegrationTiming
_ErrorInvalidTraceCnt              = ErrorInvalidTraceCnt
_ErrorTriggerSource                = ErrorTriggerSource
_ErrorInvalidTimeBase              = ErrorInvalidTimeBase
_ErrorUndefinedParameter           = ErrorUndefinedParameter
_ErrorInvalidParameter             = ErrorInvalidParameter
_ErrorNotDAQSteppedMode            = ErrorNotDAQSteppedMode
_ErrorBufAddrNotQuadDWordAlignment = ErrorBufAddrNotQuadDWordAlignment
_ErrorSignalSetup                  = ErrorSignalSetup
_ErrorDeviceIsRunning              = ErrorDeviceIsRunning
_ErrorPLLNotLocked                 = ErrorPLLNotLocked
_ErrorCLKNotPresent                = ErrorCLKNotPresent
_ErrorDDRInitFailed                = ErrorDDRInitFailed
_ErrorSelfTestFailed               = ErrorSelfTestFailed
_ErrorCalAddress                   = ErrorCalAddress
_ErrorInvalidCalBank               = ErrorInvalidCalBank
_ErrorLockMemory                   = ErrorLockMemory
_ErrorInvalidMemBank               = ErrorInvalidMemBank
_ErrorInvalidCALSource             = ErrorInvalidCALSource
_ErrorInvalidDAPolar               = ErrorInvalidDAPolar
_ErrorConfigIoctl                  = ErrorConfigIoctl
_ErrorAsyncSetIoctl                = ErrorAsyncSetIoctl
_ErrorDBSetIoctl                   = ErrorDBSetIoctl
_ErrorDBHalfReadyIoctl             = ErrorDBHalfReadyIoctl
_ErrorContOPIoctl                  = ErrorContOPIoctl
_ErrorContStatusIoctl              = ErrorContStatusIoctl
_ErrorPIOIoctl                     = ErrorPIOIoctl
_ErrorDIntSetIoctl                 = ErrorDIntSetIoctl
_ErrorWaitEvtIoctl                 = ErrorWaitEvtIoctl
_ErrorOpenEvtIoctl                 = ErrorOpenEvtIoctl
_ErrorCOSIntSetIoctl               = ErrorCOSIntSetIoctl
_ErrorMemMapIoctl                  = ErrorMemMapIoctl
_ErrorMemUMapSetIoctl              = ErrorMemUMapSetIoctl
_ErrorCTRIoctl                     = ErrorCTRIoctl
_ErrorGetResIoctl                  = ErrorGetResIoctl
