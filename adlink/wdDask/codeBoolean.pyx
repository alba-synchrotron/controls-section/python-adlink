# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


cdef class CodeBoolean(Code):
    """
Abstract class to specialise the Code class (also abstract) for the behaviour
of a boolean.
    """
    def __init__(self, *xargs, **kwargs):
        super(CodeBoolean, self).__init__(*xargs, **kwargs)

    def __bool__(self):
        return bool(self._code)

    def _setValue(self, value):
        if value in [False, 0, True, 1]:
            self._code = bool(value)
        else:
            raise ValueError("Invalid set %s. It must be a boolean" % (value))


cdef class ADDutyRestore(CodeBoolean):
    """
Code object representing a boolean to activate/deactivate the AD duty cycle
restore function.
    """
    _validValues = {False: ["False",
                            "Deactivate AD duty cycle restore function."],
                    True: ["True",
                           "Activate AD duty cycle restore function."]}

    def __init__(self, int code=1, *xargs, **kwargs):
        super(ADDutyRestore, self).__init__(code, *xargs, **kwargs)


cdef class DoubleEdged(CodeBoolean):
    """
Code object representing a boolean to enable/disable the ping-pong mode.
    """
    _validValues = {False: ["False", "Disable AD ping pong mode."],
                    True: ["True", "Enable AD ping pong mode."]}

    def __init__(self, int code=0, *xargs, **kwargs):
        super(DoubleEdged, self).__init__(code, *xargs, **kwargs)


cdef class AutoResetBuf(CodeBoolean):
    """
Code object representing a boolean to enable/disable the auto-reset mode
    """
    _validValues = {False: ["False",
                            "The AI buffers set by WD_AI_ContBufferSetup are "
                            "retained and WD_AI_ContBufferReset must be "
                            "called to reset the buffer."],
                    True: ["True",
                           "The AI buffers set WD_AI_ContBufferSetup are "
                           "reset automatically by the driver when the AI "
                           "operation is completed."]}

    def __init__(self, int code=1, *xargs, **kwargs):
        super(AutoResetBuf, self).__init__(code, *xargs, **kwargs)
