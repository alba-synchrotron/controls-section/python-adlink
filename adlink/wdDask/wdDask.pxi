# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


include "basicType.pxi"


cdef extern from "wd-dask.h":
    # Synchronous Mode
    cdef int SYNCH_OP
    cdef int ASYNCH_OP
    # ---
    cdef int WD_AI_ADCONVSRC_TimePacer
    ctypedef enum BOOLEAN:
        TRUE = 1
        FALSE = 0
    # configuration ---
    I16 WD_Register_Card(U16 CardType, U16 card_num)
    I16 WD_Release_Card(U16 CardNumber)
    I16 WD_Get_SDRAMSize(U16 CardNumber, U32 *sdramsize)
    I16 WD_GetSerialNumber(U16 wCardNumber, U8 *SerialString,
                           U8 numberOfElements, U8* actualread)
    I16 WD_GetFPGAVersion(U16 wCardNumber, U32* version)
    I16 WD_GetPXIGeographAddr(U16 wCardNumber, U8* geo_addr)  # ?
    I16 WD_AI_Config(U16 wCardNumber, U16 TimeBase, BOOLEAN adDutyRestore,
                     U16 ConvSrc, BOOLEAN doubleEdged, BOOLEAN AutoResetBuf)
    I16 WD_AI_CH_Config(U16 wCardNumber, U16 wChannel, U16 wAdRange)
    I16 WD_AI_CH_ChangeParam(U16 wCardNumber, U16 wChannel, U16 wParam,
                             U16 wValue)
    I16 WD_AI_VoltScale(U16 CardNumber, U16 AdRange, I16 reading,
                        F64 *voltage)
    I16 WD_AI_VoltScale32(U16 wCardNumber, U16 adRange, signed int reading,
                          F64 *voltage)
    # buffer ---
#     void * WD_Buffer_Alloc (U16 wCardNumber, U32 dwSize)
#     I16 WD_AI_InitialMemoryAllocated (U16 CardNumber, U32 *MemSize)
    # acquisition ---
#     I16 WD_GetActualRate(U16 wCardNumber, BOOLEAN fdir, F64 Rate,
#                          U32* interval, F64 *ActualRate)
    # trigger ---
#     I16 WD_AI_Trig_Config (U16 wCardNumber, U16 trigMode, U16 trigSrc,
#                            U16 trigPol, U16 anaTrigchan, F64 anaTriglevel,
#                            U32 postTrigScans, U32 preTrigScans,
#                            U32 trigDelayTicks, U32 reTrgCnt)
#     I16 WD_AI_Set_Mode (U16 wCardNumber, U16 modeCtrl, U16 wIter)
#     I16 WD_SoftTriggerGen(U16 wCardNumber, U8 op)
    # others ---
#     I16 WD_AI_ContReadChannel (U16 CardNumber, U16 Channel, U16 BufId,
#                                U32 ReadScans, U32 ScanIntrv, U32 SampIntrv,
#                                U16 SyncMode)
#     I16 WD_AI_ContReadMultiChannels (U16 CardNumber, U16 NumChans,
#                                      U16 *Chans, U16 BufId, U32 ReadScans,
#                                      U32 ScanIntrv, U32 SampIntrv,
#                                      U16 SyncMode)
#     I16 WD_AI_ContScanChannels (U16 CardNumber, U16 Channel, U16 BufId,
#                                 U32 ReadScans, U32 ScanIntrv, U32 SampIntrv,
#                                 U16 SyncMode)
#     I16 WD_AI_ContReadChannelToFile (U16 CardNumber, U16 Channel, U16 BufId,
#                                      U8 *FileName, U32 ReadScans,
#                                      U32 ScanIntrv, U32 SampIntrv,
#                                      U16 SyncMode)
#     I16 WD_AI_ContReadMultiChannelsToFile (U16 CardNumber, U16 NumChans,
#                                            U16 *Chans, U16 BufId,
#                                            U8 *FileName, U32 ReadScans,
#                                            U32 ScanIntrv, U32 SampIntrv,
#                                            U16 SyncMode)
#     I16 WD_AI_ContScanChannelsToFile (U16 CardNumber, U16 Channel, U16 BufId,
#                                       U8 *FileName, U32 ReadScans,
#                                       U32 ScanIntrv, U32 SampIntrv,
#                                       U16 SyncMode)
    I16 WD_AI_ContStatus (U16 CardNumber, U32 *Status)
#     I16 WD_AI_ContVScale (U16 wCardNumber, U16 adRange, void *readingArray,
#                           F64 *voltageArray, I32 count)
#     I16 WD_AI_AsyncCheck (U16 CardNumber, BOOLEAN *Stopped, U32 *AccessCnt)
#     I16 WD_AI_AsyncClear (U16 CardNumber, U32 *StartPos, U32 *AccessCnt)
#     I16 WD_AI_AsyncDblBufferHalfReady (U16 CardNumber, BOOLEAN *HalfReady,
#                                        BOOLEAN *StopFlag)
#     I16 WD_AI_AsyncDblBufferMode (U16 CardNumber, BOOLEAN Enable)
#     I16 WD_AI_AsyncDblBufferToFile (U16 CardNumber)
#     I16 WD_AI_ContBufferSetup (U16 wCardNumber, void *pwBuffer,
#                                U32 dwReadCount, U16 *BufferId)
#     I16 WD_AI_ContBufferReset (U16 wCardNumber)
#     I16 WD_AI_EventCallBack (U16 wCardNumber, I16 mode, I16 EventType,
#                              void (*callbackAddr)(int))
#     I16 WD_AI_DMA_Transfer (U16 wCardNumber, U16 BufId)
#     I16 WD_AI_ConvertCheck (U16 wCardNumber, BOOLEAN *bStopped)
#     I16 WD_AI_AsyncReStartNextReady (U16 wCardNumber, BOOLEAN *bReady,
#                                      BOOLEAN *StopFlag, U16 *RdyDaqCnt)
#     I16 WD_AI_SetTimeout (U16 wCardNumber, U32 msec)
#     I16 WD_SSI_SourceConn (U16 wCardNumber, U16 sigCode)
#     I16 WD_SSI_SourceDisConn (U16 wCardNumber, U16 sigCode)
#     I16 WD_SSI_SourceClear (U16 wCardNumber)
#     I16 WD_Route_Signal (U16 wCardNumber, U16 signal, U16 Line, U16 dir)
#     I16 WD_Signal_DisConn (U16 wCardNumber, U16 signal, U16 Line)
#
#     I16 WD_AD_Auto_Calibration_ALL(U16 wCardNumber)
#     I16 WD_EEPROM_CAL_Constant_Update(U16 wCardNumber, U16 bank)
#     I16 WD_Load_CAL_Data(U16 wCardNumber, U16 bank)


cdef extern from "wddaskex.h":
    ctypedef struct DAS_IOT_DEV_PROP:
        short card_type
        short num_of_channel
        short data_width
        short default_range
        unsigned long ctrKHz
        unsigned long bdbase
        unsigned long mask
        unsigned long maxscans
        unsigned long alignForCnt
        unsigned long reserved[13]
    short WD_GetDeviceProperties (unsigned short wCardNumber,
                                  unsigned short type,
                                  DAS_IOT_DEV_PROP* cardProp)
