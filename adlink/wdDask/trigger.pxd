# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2017, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

from adRange cimport ADRange
from cardType cimport CardType

include "trigger.pxi"


cdef class WdSoftwareTriggerGenerator(object):
    cdef:
        U16 _wCardNumber

from code cimport Code

cdef class WdTriggerMode(Code):
    pass

cdef class WdTriggerSource(Code):
    pass

cdef class WdTriggerPolarity(Code):
    pass

cdef class WdTriggerDedicatedChannel(Code):
    pass

cdef class WdTriggerLevel(object):
    cdef:
        ADRange _range
        double _value

cdef class WdTrigger(object):
    cdef:
        CardType _cardType
        U16 _wCardNumber
        WdSoftwareTriggerGenerator _swTrigger
        WdTriggerMode _mode
        WdTriggerSource _source
        WdTriggerPolarity _polarity
        WdTriggerDedicatedChannel _channel
        WdTriggerLevel _level
        int _preTriggerSamples
        int _postTriggerSamples
        int _delay
        int _triggerCounts
        str __str
        str __repr
