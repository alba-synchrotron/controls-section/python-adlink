# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2017, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


cdef extern from "wd-dask.h":
    cdef int IMPEDANCE_50Ohm
    cdef int IMPEDANCE_HI
    cdef int ADC_DITHER_DIS
    cdef int ADC_DITHER_EN
    cdef int DC_Coupling
    cdef int AC_Coupling
    cdef int BANDWIDTH_DEVICE_DEFAULT
    cdef int BANDWIDTH_20M
    cdef int BANDWIDTH_100M
    cdef int FIR_DIS
    cdef int FIR_EN_20M
    cdef int FIR_EN_10M

_IMPEDANCE_50Ohm = IMPEDANCE_50Ohm
_IMPEDANCE_HI = IMPEDANCE_HI
_ADC_DITHER_DIS = ADC_DITHER_DIS
_ADC_DITHER_EN = ADC_DITHER_EN
_DC_Coupling = DC_Coupling
_AC_Coupling = AC_Coupling
_BANDWIDTH_DEVICE_DEFAULT = BANDWIDTH_DEVICE_DEFAULT
_BANDWIDTH_20M = BANDWIDTH_20M
_BANDWIDTH_100M = BANDWIDTH_100M
_FIR_DIS = FIR_DIS
_FIR_EN_20M = FIR_EN_20M
_FIR_EN_10M = FIR_EN_10M
