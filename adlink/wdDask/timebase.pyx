# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


include "timebase.pxi"


cdef class TimeBase(Code):
    """
Code object representing the clock configuration for a card.
Find the possible values in adlink.wdDask.timebase.tb_*
    """
    _validValues = {WD_ExtTimeBase: ["tb_ExtTimeBase",
                                     "External timer is the timebase (NOT "
                                     "applicable for PCIe-9842/PXIe-9842, "
                                     "PXIe-9848)."],
                    WD_SSITimeBase: ["tb_SSITimeBase",
                                     "Timer is based on SSI source (NOT "
                                     "applicable for PCIe-9842, PXIe-9848, "
                                     "PXIe-9852, PCIe-9814)."],
                    WD_StarTimeBase: ["tb_StarTimeBase",
                                      "Clock source is from PXI_STAR line "
                                      "(NOT applicable for PXIe-9842, "
                                      "PXIe-9848)."],
                    WD_IntTimeBase: ["tb_IntTimeBase",
                                     "Internal timer is the timebase"],
                    WD_PXI_CLK10: ["PXI_CLK10",
                                   "PXI CLK is the timebase (NOT applicable "
                                   "for PXI/PXI-9820, PCIe-9842, PXIe-9848, "
                                   "PXIe-9852)."],
                    WD_PLL_REF_PXICLK10: ["tb_PLL_REF_PXICLK10",
                                          "Phase-locked loop (PLL) reference "
                                          "clock source with PXI_CLK10 as the "
                                          "source for the phase-locked loop "
                                          "(NOT applicable for "
                                          "PXI/PXI-9816/26/46, PXIe-9842)."],
                    WD_PLL_REF_EXT10: ["tb_PLL_REF_EXT10",
                                       "Phase-locked loop (PLL) reference "
                                       "clock source with an external "
                                       "reference clock as the source for the "
                                       "phase-locked loop (NOT applicable for "
                                       "PCI/PXI-9816/26/46, PCIe-9842, "
                                       "PCI-9814, PXIe-9848)."],
                    WD_PXIe_CLK100: ["tb_PXIe_CLK100",
                                     "PXIe 100M CLK is the timebase (ONLY "
                                     "available for PXIe device)."],
                    WD_PLL_REF_PXIeCLK100: ["tb_PLL_REF_PXIeCLK100",
                                            "Phase-locked loop (PLL) "
                                            "reference clock source with "
                                            "PXIe_CLK100 as the source for "
                                            "the phase-locked loop (ONLY "
                                            "available for PXIe-9848, "
                                            "PXIe-9852)."],
                    WD_DBoard_TimeBase: ["tb_DBoard_TimeBase",
                                         "Timebase signal originates with an "
                                         "embedded daughterboard (PCIe-9814 "
                                         "only)."],
                    }
    
    def __init__(self, int code=WD_IntTimeBase, *xargs, **kwargs):
        super(TimeBase, self).__init__(code, *xargs, **kwargs)

#     def _validValues(self):
#         return [WD_ExtTimeBase, WD_SSITimeBase, WD_StarTimeBase,
#                 WD_IntTimeBase, WD_PXI_CLK10, WD_PLL_REF_PXICLK10,
#                 WD_PLL_REF_EXT10, WD_PXIe_CLK100,
#                 WD_PLL_REF_PXIeCLK100, WD_DBoard_TimeBase]

#     def _setValue(self, value):
#         if value in self._validValues():
#             self._code = value
#         else:
#             raise ValueError("Invalid set %s. It must be one of "
#                              "adlink.wdDask.timebase.tb_*" % (value))
# 
#     def _interpret(self, value):
#         return {
#                 }.get(value, "No meaning for value %d" % value)
