# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2017, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

from cpython cimport bool

from cardType cimport CardType

include "wdStatus.pxi"


cdef class WdStatus(object):
    cdef:
        CardType _cardType
        U16 _cardNum
        U32 _statusWord
        dict _mask
        list _keys
        list _lowercaseKeys
        bool _dmaDone
        bool _dmaFifoEmpty
        bool _dmaFifoAlmostEmpty
        bool _dmaFifoAlmostFull
        bool _dmaFifoFull
        bool _acqInProgress
        bool _ddr2Empty
        bool _ddr2Full
        bool _localFifoEmpty
        bool _localFifoFull
        bool _triggerOccur
        bool _bufferOverrun
        bool _ddr2Overflow
