# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


include "errors.pxi"


cdef class Error(Code):
    """
Encapsulates the interpretation of a returning code from the lower level calls.
This provides a human message from what was a machine code.
    """
    _validValues = {NoError: ["NoError", "No error occurred"],
                    ErrorUnknownCardType: ["ErrorUnknownCardType",
                                           "The CardType argument is not "
                                           "valid"],
                    ErrorInvalidCardNumber: ["ErrorInvalidCardNumber",
                                             "The CardNumber argument is out "
                                             "of range (larger than 31)"],
                    ErrorTooManyCardRegistered: ["ErrorTooManyCardRegistered",
                                                 "32 cards have been "
                                                 "registered"],
                    ErrorCardNotRegistered: ["ErrorCardNotRegistered",
                                             "No card registered with id "
                                             "CardNumber"],
                    ErrorFuncNotSupport: ["ErrorFuncNotSupport",
                                          "The function called is not "
                                          "supported"],
                    ErrorInvalidIoChannel: ["ErrorInvalidIoChannel",
                                            "The specified Channel or Port "
                                            "argument is out of range"],
                    ErrorInvalidAdRange: ["ErrorInvalidAdRange",
                                          "The specified analog input range "
                                          "is invalid"],
                    ErrorContIoNotAllowed: ["ErrorContIoNotAllowed",
                                            "The specified continuous IO "
                                            "operation is not supported."],
                    ErrorDiffRangeNotSupport: ["ErrorDiffRangeNotSupport",
                                               "All analog input ranges must "
                                               "be the same for multi-channel "
                                               "analog input."],
                    ErrorLastChannelNotZero: ["ErrorLastChannelNotZero",
                                              "The channels for multi-channel "
                                              "analog input must end with or "
                                              "start from zero."],
                    ErrorChannelNotDescending: ["ErrorChannelNotDescending",
                                                "The channels for "
                                                "multi-channel analog input "
                                                "must be contiguous and in "
                                                "descending order."],
                    ErrorChannelNotAscending: ["ErrorChannelNotAscending",
                                               "The channels for "
                                               "multi-channel analog input "
                                               "must be contiguous and in "
                                               "ascending order."],
                    ErrorOpenDriverFailed: ["ErrorOpenDriverFailed",
                                            "Failed to open the device "
                                            "driver."],
                    ErrorOpenEventFailed: ["ErrorOpenEventFailed",
                                           "Open event failed in the device "
                                           "driver."],
                    ErrorTransferCountTooLarge: ["ErrorTransferCountTooLarge",
                                                 "The size of transfer is "
                                                 "larger than the size of "
                                                 "initially allocated memory "
                                                 "in the driver."],
                    ErrorNotDoubleBufferMode: ["ErrorNotDoubleBufferMode",
                                               "Double-buffer mode is "
                                               "disabled."],
                    ErrorInvalidSampleRate: ["ErrorInvalidSampleRate",
                                             "The specified sampling rate is "
                                             "out of range."],
                    ErrorInvalidCounterMode: ["ErrorInvalidCounterMode",
                                              "Value of the Mode argument is "
                                              "invalid."],
                    ErrorInvalidCounter: ["ErrorInvalidCounter",
                                          "Value of the Ctr argument is out "
                                          "of range."],
                    ErrorInvalidCounterState: ["ErrorInvalidCounterState",
                                               "Value of the State argument "
                                               "is out of range."],
                    ErrorInvalidBinBcdParam: ["ErrorInvalidBinBcdParam",
                                              "Value of the BinBcd argument "
                                              "is invalid."],
                    ErrorBadCardType: ["ErrorBadCardType",
                                       "Value of CardType argument is "
                                       "invalid."],
                    ErrorInvalidDaRefVoltage: ["ErrorInvalidDaRefVoltage",
                                               "Value of DA reference voltage "
                                               "argument is invalid."],
                    ErrorAdTimeOut: ["ErrorAdTimeOut",
                                     "AD operation timed-out."],
                    ErrorNoAsyncAI: ["ErrorNoAsyncAI",
                                     "Continuous Analog Input is not set in "
                                     "asynchronous mode"],
                    ErrorNoAsyncAO: ["ErrorNoAsyncAO",
                                     "Continuous Analog Output is not set in "
                                     "asynchronous mode."],
                    ErrorNoAsyncDI: ["ErrorNoAsyncDI",
                                     "Continuous Digital Input is not set in "
                                     "asynchronous mode."],
                    ErrorNoAsyncDO: ["ErrorNoAsyncDO",
                                     "Continuous Digital Output is not set in "
                                     "asynchronous mode."],
                    ErrorNotInputPort: ["ErrorNotInputPort",
                                        "Value of AI/DI port argument is "
                                        "invalid."],
                    ErrorNotOutputPort: ["ErrorNotOutputPort",
                                         "Value of AO/DO argument is "
                                         "invalid."],
                    ErrorInvalidDioPort: ["ErrorInvalidDioPort",
                                          "Value of DI/O port argument is "
                                          "invalid."],
                    ErrorInvalidDioLine: ["ErrorInvalidDioLine",
                                          "Value of DI/O line argument is "
                                          "invalid."],
                    ErrorContIoActive: ["ErrorContIoActive",
                                        "Continuous IO operation is not "
                                        "active."],
                    ErrorDblBufModeNotAllowed: ["ErrorDblBufModeNotAllowed",
                                                "Double Buffer mode is not "
                                                "allowed"],
                    ErrorConfigFailed: ["ErrorConfigFailed",
                                        "The specified function configuration "
                                        "failed."],
                    ErrorInvalidPortDirection: ["ErrorInvalidPortDirection",
                                                "The value of DIO port "
                                                "direction argument is "
                                                "invalid."],
                    ErrorBeginThreadError: ["ErrorBeginThreadError",
                                            "Failed to create thread."],
                    ErrorInvalidPortWidth: ["ErrorInvalidPortWidth",
                                            "The port width setting is not "
                                            "allowed."],
                    ErrorInvalidCtrSource: ["ErrorInvalidCtrSource",
                                            "The clock source setting is "
                                            "invalid."],
                    ErrorOpenFile: ["ErrorOpenFile",
                                    "Failed to open file."],
                    ErrorAllocateMemory: ["ErrorAllocateMemory",
                                          "Memory allocation failed."],
                    ErrorDaVoltageOutOfRange: ["ErrorDaVoltageOutOfRange",
                                               "Value of DA voltage argument "
                                               "is out of range."],
                    ErrorInvalidSyncMode: ["ErrorInvalidSyncMode",
                                           "The sync. mode of operation is "
                                           "invalid."],
                    ErrorInvalidBufferID: ["ErrorInvalidBufferID",
                                           "The buffer id selected is "
                                           "invalid."],
                    ErrorInvalidCNTInterval: ["ErrorInvalidCNTInterval",
                                              "The counter value is invalid."],
                    ErrorReTrigModeNotAllowed: ["ErrorReTrigModeNotAllowed",
                                                "The re-trigger mode of "
                                                "operation is invalid."],
                    ErrorResetBufferNotAllowed: ["ErrorResetBufferNotAllowed",
                                                 "The buffer is not allowed "
                                                 "to be reset."],
                    ErrorAnaTrggerLevel: ["ErrorAnaTrggerLevel",
                                          "The value of analog trigger level "
                                          "is invalid."],
                    ErrorDAQEvent: ["ErrorDAQEvent",
                                    "The DAQEvent is invalid."],
                    ErrorInvalidDataSize: ["ErrorInvalidDataSize",
                                           "The data size is invalid. For "
                                           "example, the data size of "
                                           "pre-trigger and middle trigger "
                                           "must be a multiple of 32."],
                    ErrorOffsetCalibration: ["ErrorOffsetCalibration",
                                             "The AD offset calibration "
                                             "failed."],
                    ErrorGainCalibration: ["ErrorGainCalibration",
                                           "The AD gain calibration failed."],
                    ErrorCountOutofSDRAMSize: ["ErrorCountOutofSDRAMSize",
                                               "The data count is out of "
                                               "size for the onboard SDRAM or "
                                               "DDR2."],
                    ErrorNotStartTriggerModule: ["ErrorNotStartTriggerModule",
                                                 "The module is not installed "
                                                 "in a start trigger slot."],
                    ErrorInvalidRouteLine: ["ErrorInvalidRouteLine",
                                            "Value of routing line is "
                                            "invalid."],
                    ErrorInvalidSignalCode: ["ErrorInvalidSignalCode",
                                             "Value of signal code is "
                                             "invalid."],
                    ErrorInvalidSignalDirection: ["ErrorInvalidSignal"
                                                  "Direction",
                                                  "The signal connection "
                                                  "direction is invalid."],
                    ErrorTRGOSCalibration: ["ErrorTRGOSCalibration",
                                            "The analog trigger level offset "
                                            "calibration failed."],
                    ErrorNoSDRAM: ["ErrorNoSDRAM",
                                   "No onboard SDRAM or DDR2"],
                    ErrorIntegrationGain: ["ErrorIntegrationGain",
                                           "The value of Integration Gain is "
                                           "invalid"],
                    ErrorAcquisitionTiming: ["ErrorAcquisitionTiming",
                                             "The value of acquisition time "
                                             "is invalid"],
                    ErrorIntegrationTiming: ["ErrorIntegrationTiming",
                                             "The value of integration time "
                                             "is invalid"],
                    ErrorInvalidTraceCnt: ["ErrorInvalidTraceCnt",
                                           "The value of trace count is "
                                           "invalid"],
                    ErrorTriggerSource: ["ErrorTriggerSource",
                                         "The selected trigger source is "
                                         "invalid"],
                    ErrorInvalidTimeBase: ["ErrorInvalidTimeBase",
                                           "The selected TimeBase is invalid"],
                    ErrorUndefinedParameter: ["ErrorUndefinedParameter",
                                              "One or more parameters are "
                                              "undefined"],
                    ErrorInvalidParameter: ["ErrorInvalidParameter",
                                            "One or more parameters are "
                                            "invalid"],
                    ErrorNotDAQSteppedMode: ["ErrorNotDAQSteppedMode",
                                             "The DAQ Stepped mode is not "
                                             "set"],
                    ErrorBufAddrNotQuadDWordAlignment: ["ErrorBufAddrNotQuad"
                                                        "DWordAlignment",
                                                        "The buffer address "
                                                        "is not in QuadDWord "
                                                        "Alignment"],
                    ErrorSignalSetup: ["ErrorSignalSetup",
                                       ""],  # ***
                    ErrorDeviceIsRunning: ["ErrorDeviceIsRunning",
                                           "Operation cannot be performed "
                                           "while device is running"],
                    ErrorPLLNotLocked: ["ErrorPLLNotLocked",
                                        "PLL could not phase-lock to the "
                                        "reference clock"],
                    ErrorCLKNotPresent: ["ErrorCLKNotPresent",
                                         "The selected clock source or "
                                         "timebase is not present"],
                    ErrorDDRInitFailed: ["ErrorDDRInitFailed",
                                         "Undocumented"],  # ***
                    ErrorSelfTestFailed: ["ErrorSelfTestFailed",
                                          "Undocumented"],  # ***
                    ErrorCalAddress: ["ErrorCalAddress",
                                      "The calibration address is invalid"],
                    ErrorInvalidCalBank: ["ErrorInvalidCalBank",
                                          "The selected calibration bank is "
                                          "invalid"],
                    ErrorLockMemory: ["ErrorLockMemory",
                                      "Undocumented"],  # ***
                    ErrorInvalidMemBank: ["ErrorInvalidMemBank",
                                          "Undocumented"],  # ***
                    ErrorInvalidCALSource: ["ErrorInvalidCALSource",
                                            "Undocumented"],  # ***
                    ErrorInvalidDAPolar: ["ErrorInvalidDAPolar",
                                          "Undocumented"],  # ***
                    ErrorConfigIoctl: ["ErrorConfigIoctl",
                                       "The configuration API failed."],
                    ErrorAsyncSetIoctl: ["ErrorAsyncSetIoctl",
                                         "The async. mode API failed."],
                    ErrorDBSetIoctl: ["ErrorDBSetIoctl",
                                      "The double-buffer setting API failed."],
                    ErrorDBHalfReadyIoctl: ["ErrorDBHalfReadyIoctl",
                                            "The half-ready API failed."],
                    ErrorContOPIoctl: ["ErrorContOPIoctl",
                                       "The continuous data acquisition API "
                                       "failed."],
                    ErrorContStatusIoctl: ["ErrorContStatusIoctl",
                                           "The continuous data acquisition "
                                           "status API setting failed."],
                    ErrorPIOIoctl: ["ErrorPIOIoctl",
                                    "The polling data API failed."],
                    ErrorDIntSetIoctl: ["ErrorDIntSetIoctl",
                                        "The dual interrupt setting API "
                                        "failed."],
                    ErrorWaitEvtIoctl: ["ErrorWaitEvtIoctl",
                                        "The wait event API failed."],
                    ErrorOpenEvtIoctl: ["ErrorOpenEvtIoctl",
                                        "The open event API failed."],
                    ErrorCOSIntSetIoctl: ["ErrorCOSIntSetIoctl",
                                          "The COS interrupt setting API "
                                          "failed."],
                    ErrorMemMapIoctl: ["ErrorMemMapIoctl",
                                       "The memory mapping API failed."],
                    ErrorMemUMapSetIoctl: ["ErrorMemUMapSetIoctl",
                                           "The memory unmapping API failed."],
                    ErrorCTRIoctl: ["ErrorCTRIoctl",
                                    "The counter API failed."],
                    ErrorGetResIoctl: ["ErrorGetResIoctl",
                                       "The resource getting API failed."],
                    }

    def __cinit__(self, *xargs, **kwargs):
        super(Error, self).__init__(*xargs, **kwargs)
