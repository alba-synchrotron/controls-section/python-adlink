# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2017, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


from errors cimport Error
import cardType


include "wdStatus.pxi"


masks = {9852: {'DMA_done': [0, '_dmaDone'],
               'DMA_FIFO_empty': [1, '_dmaFifoEmpty'],
               'DMA_FIFO_almost_empty': [2, '_dmaFifoAlmostEmpty'],
               'DMA_FIFO_almost_full': [3, '_dmaFifoAlmostFull'],
               'DMA_FIFO_full': [4, '_dmaFifoFull'],
               'AcqInProgress': [8, '_acqInProgress'],
               'DDR2_empty': [9, '_ddr2Empty'],
               'DDR2_full': [10, '_ddr2Full'],
               'Local_FIFO_empty': [11, '_localFifoEmpty'],
               'Local_FIFO_full': [12, '_localFifoFull'],
               'TriggerOccur': [13, '_triggerOccur'],
               'Buffer_Overrun': [29, '_bufferOverrun'],
               'DDR2_Overflow': [30, '_ddr2Overflow']
}}


cdef class WdStatus(object):
    """
The object reports the card activity to summarise if the card is busy doing
what or it is prepared to acquire.

Given a WdDask object (build for an specific card type and sequence number):
>>> import adlink
>>> factory = adlink.Factory()
>>> cardObj = factory.getCard('9852', 0)

The user may require the summary of the card status:
>>> cardObj.status.summary
 {'AcqInProgress': False,
  'Buffer_Overrun': False,
  'DDR2_Overflow': False,
  'DDR2_empty': True,
  'DDR2_full': False,
  'DMA_FIFO_almost_empty': False,
  'DMA_FIFO_almost_full': False,
  'DMA_FIFO_empty': True,
  'DMA_FIFO_full': False,
  'DMA_done': False,
  'Local_FIFO_empty': True,
  'Local_FIFO_full': False,
  'TriggerOccur': False}

All those keys in the summary are also attributes of the status object, then
they can be requested directly like:
>>> cardObj.status.acqInProgress
 False
>>> cardObj.status.triggerOccur
 False
    """
    def __cinit__(self, U16 cardType, U16 cardNum):
        self._cardType = CardType(cardType)
        self._cardNum = cardNum

    def __init__(self, cardType, cardNum, *args, **kwargs):
        """
            Input: U16 cardType, U16 cardNum
        """
        super(WdStatus, self).__init__(*args, **kwargs)
        print("WdStatus(cardType %d, cardNum %d)"
              % (self._cardType, self._cardNum))
        self._mask = self.__getCardMask()
        self._keys = list(self._mask.keys())
        self._lowercaseKeys = [x.lower() for x in self._keys]
        self.__getStatusWord()

    def __str__(self):
        return "WdStatus(%s, %d)" % (self._cardType.string, self._cardNum)

    def __repr__(self):
        return "WdStatus(%s, %d)" % (self._cardType.human, self._cardNum)

    def __int__(self):
        return self.__getStatus()

    def __getCardMask(self):
        if self._cardType in [cardType._PCIe_9852, cardType._PXIe_9852]:
            return masks[9852]
        return {}

    def __getStatusWord(self):
        error = Error(WD_AI_ContStatus(self._cardNum, &self._statusWord))
        if int(error) != 0:
            raise Exception("%r" % error)
        return <long>self._statusWord

    def keys(self):
        return self._keys

    def __getitem__(self, key):
        self.__getStatusWord()
        key = key.lower()
        if key not in self._lowercaseKeys:
            raise KeyError("'%s' card doesn't report about %r"
                           % (self._cardType.human, key))
        key = self._keys[self._lowercaseKeys.index(key)]
        bit = self._mask[key][0]
        return bool(self._statusWord & 1 << bit)

    @property
    def summary(self):
        self.__getStatusWord()
        def mask(bit):
            return self._statusWord & 1<<bit
        if len(self._keys) == 0:
            raise NotImplementedError("Not yet available for this card")
        answer = {}
        for key in self._keys:
            bit = self._mask[key][0]
            value = bool(mask(bit))
            internalVbleName = self._mask[key][1]
            if hasattr(self, internalVbleName):
                internalVble = getattr(self, internalVbleName)
                internalVble = value
            answer[key] = value
        return answer

    @property
    def dmaDone(self):
        try:
            return self.__getitem__('dma_Done')
        except:
            return None

    @property
    def dmaFifoEmpty(self):
        try:
            return self.__getitem__('dma_Fifo_Empty')
        except:
            return None

    @property
    def dmaFifoAlmostEmpty(self):
        try:
            return self.__getitem__('dma_Fifo_Almost_Empty')
        except:
            return None

    @property
    def dmaFifoAlmostFull(self):
        try:
            return self.__getitem__('dma_Fifo_Almost_Full')
        except:
            return None

    @property
    def dmaFifoFull(self):
        try:
            return self.__getitem__('dma_Fifo_Full')
        except:
            return None

    @property
    def acqInProgress(self):
        try:
            return self.__getitem__('acqInProgress')
        except:
            return None

    @property
    def ddr2Empty(self):
        try:
            return self.__getitem__('ddr2_Empty')
        except:
            return None

    @property
    def ddr2Full(self):
        try:
            return self.__getitem__('ddr2_Full')
        except:
            return None

    @property
    def localFifoEmpty(self):
        try:
            return self.__getitem__('local_Fifo_Empty')
        except:
            return None

    @property
    def localFifoFull(self):
        try:
            return self.__getitem__('local_Fifo_Full')
        except:
            return None

    @property
    def triggerOccur(self):
        try:
            return self.__getitem__('triggerOccur')
        except:
            return None

    @property
    def bufferOverrun(self):
        try:
            return self.__getitem__('buffer_Overrun')
        except:
            return None

    @property
    def ddr2Overflow(self):
        try:
            return self.__getitem__('ddr2_Overflow')
        except:
            return None
