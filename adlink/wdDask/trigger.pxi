# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2017, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


include "basicType.pxi"


cdef extern from "wd-dask.h":
    I16 WD_SoftTriggerGen(U16 wCardNumber, U8 op)
    # trigger mode
    cdef int WD_AI_TRGMOD_POST
    cdef int WD_AI_TRGMOD_PRE
    cdef int WD_AI_TRGMOD_MIDL
    cdef int WD_AI_TRGMOD_DELAY
    # trigger source
    cdef int WD_AI_TRGSRC_SOFT
    cdef int WD_AI_TRGSRC_ANA
    cdef int WD_AI_TRGSRC_ExtD
    cdef int WD_AI_TRSRC_SSI_1
    cdef int WD_AI_TRSRC_SSI_2
    cdef int WD_AI_TRSRC_PXIStar
    cdef int WD_AI_TRSRC_PXIeStart
    # trigger polarity
    cdef int WD_AI_TrgPositive
    cdef int WD_AI_TrgNegative
    # analog trigger Dedicated Channel
    cdef int CH0ATRIG
    cdef int CH1ATRIG
    cdef int CH2ATRIG
    cdef int CH3ATRIG
    cdef int CH4ATRIG
    cdef int CH5ATRIG
    cdef int CH6ATRIG
    cdef int CH7ATRIG
    I16 WD_AI_Trig_Config(U16 wCardNumber, U16 trigMode, U16 trigSrc,
                          U16 trigPol, U16 anaTrigchan, F64 anaTriglevel,
                          U32 postTrigScans, U32 preTrigScans,
                          U32 trigDelayTicks, U32 reTrgCnt)

_WD_AI_TRGMOD_POST = WD_AI_TRGMOD_POST
_WD_AI_TRGMOD_PRE = WD_AI_TRGMOD_PRE
_WD_AI_TRGMOD_MIDL = WD_AI_TRGMOD_MIDL
_WD_AI_TRGMOD_DELAY = WD_AI_TRGMOD_DELAY

_WD_AI_TRGSRC_SOFT = WD_AI_TRGSRC_SOFT
_WD_AI_TRGSRC_ANA = WD_AI_TRGSRC_ANA
_WD_AI_TRGSRC_ExtD = WD_AI_TRGSRC_ExtD
_WD_AI_TRSRC_SSI_1 = WD_AI_TRSRC_SSI_1
_WD_AI_TRSRC_SSI_2 = WD_AI_TRSRC_SSI_2
_WD_AI_TRSRC_PXIStar = WD_AI_TRSRC_PXIStar
_WD_AI_TRSRC_PXIeStart = WD_AI_TRSRC_PXIeStart

_WD_AI_TrgPositive = WD_AI_TrgPositive
_WD_AI_TrgNegative = WD_AI_TrgNegative
