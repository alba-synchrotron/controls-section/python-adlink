# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


include "adRange.pxi"


cdef class ADRange(Code):
    """
Analog input ranges of digitaliser cards. Find the possible values in
adlink.wdDask.adRange.ad_*
    """
    
    _validValues = {AD_B_10_V: ["AD_B_10_V", "Bipolar -10V to +10V"],
                    AD_B_5_V: ["AD_B_5_V", "Bipolar -5V to +5V"],
                    AD_B_2_5_V: ["AD_B_2_5_V", "Bipolar -2.5V to +2.5V"],
                    AD_B_1_25_V: ["AD_B_1_25_V", "Bipolar -1.25V to +1.25V"],
                    AD_B_0_625_V: ["AD_B_0_625_V",
                                   "Bipolar -0.625V to +0.625V"],
                    AD_B_0_3125_V: ["AD_B_0_3125_V",
                                    "Bipolar -0.3125V to +0.3125V"],
                    AD_B_0_5_V: ["AD_B_0_5_V", "Bipolar -0.5V to +0.5V"],
                    AD_B_0_05_V: ["AD_B_0_05_V", "Bipolar -0.05V to +0.05V"],
                    AD_B_0_005_V: ["AD_B_0_005_V",
                                   "Bipolar -0.005V to +0.005V"],
                    AD_B_1_V: ["AD_B_1_V", "Bipolar -1V to +1V"],
                    AD_B_0_1_V: ["AD_B_0_1_V", "Bipolar -0.1V to +0.1V"],
                    AD_B_0_01_V: ["AD_B_0_01_V", "Bipolar -0.01V to +0.01V"],
                    AD_B_0_001_V: ["AD_B_0_001_V",
                                   "Bipolar -0.001V to +0.001V"],
                    AD_U_20_V: ["AD_U_20_V", "Unipolar 0 to +20V"],
                    AD_U_10_V: ["AD_U_10_V", "Unipolar 0 to +10V"],
                    AD_U_5_V: ["AD_U_5_V", "Unipolar 0 to +5V"],
                    AD_U_2_5_V: ["AD_U_2_5_V", "Unipolar 0 to +2.5V"],
                    AD_U_1_25_V: ["AD_U_1_25_V", "Unipolar 0 to +1.25V"],
                    AD_U_1_V: ["AD_U_1_V", "Unipolar 0 to +1V"],
                    AD_U_0_1_V: ["AD_U_0_1_V", "Unipolar 0 to +0.1V"],
                    AD_U_0_01_V: ["AD_U_0_01_V", "Unipolar 0 to +0.01V"],
                    AD_U_0_001_V: ["AD_U_0_001_V", "Unipolar 0 to +0.001V"],
                    AD_B_2_V: ["AD_B_2_V", "Bipolar -2V to +2V"],
                    AD_B_0_25_V: ["AD_B_0_25_V", "Bipolar -0.25V to +0.25V"],
                    AD_B_0_2_V: ["AD_B_0_2_V", "Bipolar -0.2V to +0.2V"],
                    AD_U_4_V: ["AD_U_4_V", "Unipolar 0 to +4V"],
                    AD_U_2_V: ["AD_U_2_V", "Unipolar 0 to +2V"],
                    AD_U_0_5_V: ["AD_U_0_5_V", "Unipolar 0 to +0.5V"],
                    AD_U_0_4_V: ["AD_U_0_4_V", "Unipolar 0 to +0.4V"],
                    AD_B_1_5_V: ["AD_B_1_5_V", "Bipolar -1.5V to +1.5V"],
                    AD_B_0_2145_V: ["AD_B_0_2145_V", "Bipolar -0.2145V to "
                                    "+0.2145V"],
                    }
    
    def __init__(self, int code=AD_B_10_V, *xargs, **kwargs):
        super(ADRange, self).__init__(code, *xargs, **kwargs)

