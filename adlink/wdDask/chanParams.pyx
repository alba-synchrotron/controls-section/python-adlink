# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


include "chanParams.pxi"


from adRange cimport ADRange
from code cimport Impedance, AdcDither, Coupling, Bandwidth, SignalFir


cdef class ChannelParameter(object):
    """
Encapsulates the behaviour and constrains for the parameters to set up a
channel configuration. This objects saves a relation between an specific code
instance and how a channel needs the information when the configuration has
to change.
    """
    __parameter__ = {"ai_range": {"code":_AI_RANGE,
                                  "klass": ADRange},
                     "ai_impedance": {"code": _AI_IMPEDANCE,
                                      "klass": Impedance},
                     "adc_dither": {"code":_ADC_DITHER,
                                    "klass": AdcDither},
                     "ai_coupling": {"code": _AI_COUPLING,
                                     "klass": Coupling},
                     "adc_bandwidth": {"code": _ADC_Bandwidth,
                                       "klass": Bandwidth},
                     "signal_fir": {"code": _SIGNAL_FIR,
                                    "klass": SignalFir}}

    def __init__(self, wParam, U16 wValue):
        if wParam.lower() not in self.__parameter__.keys():
            raise AttributeError("Unrecognised parameter %r" % (wParam))
        self._code = self.__parameter__[wParam.lower()]["klass"](wValue)
        self._wParam = self.__parameter__[wParam.lower()]["code"]
        self.value = wValue

    def __str__(self):
        return "ChannelParameter(%r)" % (self._code)

    def __repr__(self):
        return "ChannelParameter(%r, parameter %d, code %d)"\
            % (self._code, self._wParam, self.value)

    @property
    def parameter(self):
        return self._wParam

    @property
    def value(self):
        return self._wValue

    @value.setter
    def value(self, value):
        if value not in self._code.validValues():
            raise AttributeError("Invalid value")
        self._wValue = value
        self._code.code = value

    @property
    def code(self):
        return self._code
