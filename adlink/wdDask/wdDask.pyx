# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


from code cimport Code

from codeBoolean cimport ADDutyRestore, DoubleEdged, AutoResetBuf
from adRange cimport ADRange
from chanParams cimport ChannelParameter
from errors cimport Error
from trigger cimport WdTrigger
from wdBuffers cimport WdBufferFactory
from wdChannels cimport WdChannels, WdChannel


include "wdDask.pxi"


cdef class WdDask(object):
    """
Constructor class to interface Adlink cards controlled by the wd-dask.

Objects from this class shall be obtained using the module Factory()

Some identification information can be requested to this object to know about
the card it represents.

>>> card.cardType
51:[PCIe_9852] PCI Express 9852
>>> card.cardNum
0

This information identifies the card as the first in the bus of the PCI
Express 9852 model. It may help also the 'card.serialNumber' information.

The number of channels a card have can be known by:
>>> card.nChannels
2
Then, the object returned by 'card.channels' represents the group of they form.
They can be individually accessed by requeting it to this object or to the
card directly:
>>> ch0 = card.channels[0]
>>> ch1 = card.channel(1)

Those channels have a set of parameters that may be equal or not for all of
them. It is possible to change the impedance to only one of them:
>>> ch0.impedance = adlink.wdDask.code._IMPEDANCE_HI
>>> card.channels.parameters
{'adc_bandwidth': 0:[BANDWIDTH_DEVICE_DEFAULT] Default,
 'adc_dither': 0:[ADC_DITHER_DIS] ADC Dither Disabled,
 'ai_coupling': 0:[DC_Coupling] DC,
 'impedance': [1:[IMPEDANCE_HI] 1M Ohm, 0:[IMPEDANCE_50Ohm] 50 Ohm],
 'signal_fir': 0:[FIR_DIR] Signal FIR disable}

or to both channels in a single operation:
>>> card.channels.ai_coupling = adlink.wdDask.code._AC_Coupling

The values on this parameter dictionary are a single ChannelParameter object
when all the channels have the same configuration, or a sorted list when
they have different configurations.
    """
    def __cinit__(self, U16 cardType, U16 cardNum):
        # print("WdDask().__cinit__(%d, %d)" % (cardType, cardNum))
        self._cardType = CardType(cardType)
        self._cardNum = cardNum
        self._timebase = TimeBase()
        self._adDutyRestore = ADDutyRestore()
        self._doubleEdged = DoubleEdged()
        self._autoResetBuf = AutoResetBuf()
        self._adRange = ADRange()
        self._trigger = WdTrigger(self._cardType, self._cardNum, self._adRange)
        self.__registerCard()
        self.__getSerialNumber()
        self.__AI_Config()

    def __init__(self, cardType, cardNum, *args, **kwargs):
        """
            Input: U16 cardType, U16 cardNum
        """
        super(WdDask, self).__init__(*args, **kwargs)
        print("WdDask(cardType %d, cardNum %d)"
              % (self._cardType, self._cardNum))
        self._status = WdStatus(self._cardType, self._cardNum)
        self.__properties = {}
        self.__channels = None
        self.__getCardProperties()
        self.__scan4channels()
        self.__prepareBufferFactory()

    def __str__(self):
        return "WdDask(%s, %d)" % (self._cardType.string, self._cardNum)

    def __repr__(self):
        return "WdDask(%s, %d)" % (self._cardType.human, self._cardNum)

    def __richcmp__(self, other, int op):
        if op in [2, 3]:  # ==, !=
            if self.cardType == other.cardType and\
                    self.cardNum == other.cardNum:
                return True if op == 2 else False
            return False if op == 2 else False
        return False

    def __registerCard(self):
        error = Error(WD_Register_Card(<U16>self._cardType, self._cardNum))
        print("WD_Register_Card() -> %r" % (error))
        if int(error) != 0:
            raise Exception("%r" % error)
        self._registered = True

    def __dealloc__(self):
        self.__releaseCard()

    def __releaseCard(self):
        error = Error(WD_Release_Card(self._cardNum))
        print("WD_Release_Card() -> %r" % (error))
        if int(error) != 0:
            raise Exception(error)
        self._registered = False

    ######################
    # Card information ---
    @property
    def serialNumber(self):
        """Returns the serial number of the card that must match with the
           number listed in teh device label.
        """
        return self._serialNumber

    def __getSerialNumber(self):
        cdef:
            U8 SerialString[20]
            U8 numberOfElements = 20
            U8 actualread = 0
        error = Error(WD_GetSerialNumber(self._cardNum, SerialString,
                                         numberOfElements, &actualread))
        # print("WD_GetSerialNumber() -> %r" % (error))
        if int(error) != 0:
            raise Exception("%r" % error)
        # print("actualread %d" % (<int>actualread))
        self._serialNumber = self.__interpretSerialNumber(SerialString,
                                                          actualread)

    cdef str __interpretSerialNumber(self, U8 *SerialString, U8 actualread):
        # FIXME: review the getSN example, because this maybe very different
        serialNumber = 0
        for i in range(actualread):
            # print("%d: %x (%d)" % (i, SerialString[i], SerialString[i]))
            serialNumber *= 10
            serialNumber += SerialString[i]
        # print("s/n: %x (%d)" % (serialNumber, serialNumber))
        return hex(serialNumber)

    @property
    def isCardRegistered(self):
        return self._registered

    @property
    def cardType(self):
        """Card identification in this python-adlink module.
        """
        return self._cardType

    @property
    def cardNum(self):
        """Cardinal position of the card of the same model.
        """
        return self._cardNum

    def SDRAMSize(self):
        """Get the size of ram in the card in MB.
        """
        # TODO: filter if card is "registered"
        cdef U32 sdramsize = -1
        error = Error(WD_Get_SDRAMSize(self._cardNum, &sdramsize))
        # print("WD_Get_SDRAMSize() -> %r" % (error))
        if int(error) != 0:
            raise Exception("%r" % error)
        return <long>sdramsize

    def FPGAVersion(self):
        """Get the firmware version in the card.
        """
        cdef U32 FPGAVersion = -1
        error = Error(WD_GetFPGAVersion(self._cardNum, &FPGAVersion))
        # print("WD_GetFPGAVersion() -> %r" % (error))
        if int(error) != 0:
            raise Exception("%r" % error)
        return <long>FPGAVersion

    def PXIGeographAddr(self):
        """The number of physical slot where the PXI module is installed.
        """
        cdef U8 geo_addr = -1
        error = Error(WD_GetPXIGeographAddr(self._cardNum, &geo_addr))
        # print("WD_GetPXIGeographAddr() -> %r" % (error))
        if int(error) != 0:
            raise Exception("%r" % error)
        return <char>geo_addr

    def __getCardProperties(self):
        cdef:
            DAS_IOT_DEV_PROP properties
        WD_GetDeviceProperties(self._cardNum, 0, &properties)  # type 0: 'ai'
        self.__properties['nChannels'] = properties.num_of_channel
        self.__properties['dataWidth'] = properties.data_width
        self.__properties['defaultRange'] = properties.default_range
        self.__properties['ctrKHz'] = properties.ctrKHz
        self.__properties['bdbase'] = properties.bdbase
        self.__properties['mask'] = properties.mask
        self.__properties['maxscans'] = properties.maxscans
        self.__properties['alignForCnt'] = properties.alignForCnt
        self.__properties['reserved'] = properties.reserved

    @property
    def cardProperties(self):
        return self.__properties

    @property
    def nChannels(self):
        return self.__properties['nChannels']

    @property
    def dataWitdh(self):
        return self.__properties['dataWidth']

    @property
    def status(self):
        return self._status
    # card information done ---
    ###########################

    ########################
    # card configuration ---
    @property
    def timebase(self):
        """Code object with the clock configuration in the card.
           Assign a value from the constants located in:
           adlink.wdDask.timebase.tb_*
        """
        return self._timebase

    @timebase.setter
    def timebase(self, U16 value):
        self._timebase.code = value
        self.__AI_Config()

    @property
    def adDutyRestore(self):
        """Code object representing a boolean to activate/deactivate
           the AD duty cycle restore function.
        """
        return self._adDutyRestore

    @adDutyRestore.setter
    def adDutyRestore(self, value):
        self._adDutyRestore.code = value
        self.__AI_Config()

    @property
    def doubleEdge(self):
        """Code object representing a boolean to enable/disable
           the ping-pong mode.
        """
        return self._doubleEdged

    @doubleEdge.setter
    def doubleEdge(self, value):
        self._doubleEdged.code = value
        self.__AI_Config()

    @property
    def autoResetBuf(self):
        """Code object representing a boolean to enable/disable
           the auto-reset mode
        """
        return self._autoResetBuf

    @autoResetBuf.setter
    def autoResetBuf(self, value):
        self._autoResetBuf.code = value
        self.__AI_Config()

    def __AI_Config(self):
        error = Error(WD_AI_Config(self._cardNum, int(self.timebase),
                                   bool(self.adDutyRestore),
                                   WD_AI_ADCONVSRC_TimePacer,
                                   bool(self._doubleEdged),
                                   bool(self._autoResetBuf)))
        print("WD_AI_Config() -> %r" % (error))
        if int(error) != 0:
            raise Exception("%r" % error)
    # card configuration done ---
    #############################

    ###################
    # input voltage ---
    @property
    def analogInputRange(self):
        return self._adRange

    @analogInputRange.setter
    def analogInputRange(self, value):
        self._adRange.code = value

    # TODO: assign the ranges by a list:
    # >>> cardObj.analogInputRange = [-10, 10]
    # equivalent to:
    # >>> cardObj.analogInputRange = adlink.wdDask.adRange.ad_B_10_V

    @property
    def voltageScale(self):
        """Converts AD data to actual input voltage
        """
        return self.__AI_VoltScale(self._adRange)[1]

    @property
    def voltageScaleRaw(self):
        """Raw reading of AD data to actual input voltage
        """
        return self.__AI_VoltScale(self._adRange)[0]

    @property
    def voltageScale32(self):
        """Converts AD data to actual input voltage
        """
        return self.__AI_VoltScale32(self._adRange)[1]

    @property
    def voltageScale32Raw(self):
        """Raw reading of AD data to actual input voltage
        """
        return self.__AI_VoltScale32(self._adRange)[0]

    def __AI_VoltScale(self, U16 AdRange):
        cdef:
            I16 reading = 0
            F64 voltage = 0.0
        print("AdRange: %d" % AdRange)
        error = Error(WD_AI_VoltScale(self._cardNum, AdRange,
                                      reading, &voltage))
        print("WD_AI_VoltScale() -> %r" % (error))
        if int(error) != 0:
            raise Exception("%r" % error)
        return <int>reading, <double>voltage

    def __AI_VoltScale(self, ADRange adRange):
        return self.AI_VoltScale(int(adRange))

    def __AI_VoltScale32(self, U16 AdRange):
        cdef:
            I32 reading = 0
            F64 voltage = 0.0
        error = Error(WD_AI_VoltScale(self._cardNum, AdRange,
                                      reading, &voltage))
        print("WD_AI_VoltScale() -> %r" % (error))
        if int(error) != 0:
            raise Exception("%r" % error)
        return <long>reading, <double>voltage

    def __AI_VoltScale32(self, ADRange adRange):
        return self.AI_VoltScale32(int(adRange))
    # input voltage done ---
    ########################

    #############
    # trigger ---
    @property
    def triggerObj(self):
        """ Object to manage the trigger configuration for the current card.
        """
        return self._trigger
    
#     @property
#     def softwareTriggerObj(self):
#         """Object to manage the software trigger generator object.
#            Mainly the returned object has a method 'emit' that requires an
#            operation number as parameter to generate the corresponding trigger.
#         """
#         return self._swTrigger
    # trigger done ---
    ##################

    def __scan4channels(self):
        if self.__channels is None:
            # print("__scan4channels() for %d channels" % (self.nChannels))
            self.__channels = WdChannels()
            for i in range(self.nChannels):
                self.__channels[i] = WdChannel(self, i)

    def channel(self, i):
        return self.__channels[i]

    @property
    def channels(self):
        return self.__channels

    def __prepareBufferFactory(self):
        self.__bufferFactory = WdBufferFactory(self)

    @property
    def _bufferFactoryObj(self):
        return self.__bufferFactory

    @property
    def activeBuffers(self):
        return self.__bufferFactory.activeBuffers

#     def newBuffer(self, nSamples):
#         idx = self.__bufferFactory._setupBuffer(nSamples)
#         return self.__bufferFactory[idx]

    # def releaseBuffer(self, idx):
        
