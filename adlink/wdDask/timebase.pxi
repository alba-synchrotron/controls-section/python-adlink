# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


cdef extern from "wd-dask.h":
    cdef int WD_ExtTimeBase
    cdef int WD_SSITimeBase
    cdef int WD_StarTimeBase
    cdef int WD_IntTimeBase
    cdef int WD_PXI_CLK10
    cdef int WD_PLL_REF_PXICLK10
    cdef int WD_PLL_REF_EXT10
    cdef int WD_PXIe_CLK100
    cdef int WD_PLL_REF_PXIeCLK100
    cdef int WD_DBoard_TimeBase

tb_ExtTimeBase        = WD_ExtTimeBase
tb_SSITimeBase        = WD_SSITimeBase
tb_StarTimeBase       = WD_StarTimeBase
tb_IntTimeBase        = WD_IntTimeBase
tb_PXI_CLK10          = WD_PXI_CLK10
tb_PLL_REF_PXICLK10   = WD_PLL_REF_PXICLK10
tb_PLL_REF_EXT10      = WD_PLL_REF_EXT10
tb_PXIe_CLK100        = WD_PXIe_CLK100
tb_PLL_REF_PXIeCLK100 = WD_PLL_REF_PXIeCLK100
tb_DBoard_TimeBase    = WD_DBoard_TimeBase
