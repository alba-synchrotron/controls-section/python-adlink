# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

# from cpython cimport bool

from adRange cimport ADRange
from chanParams cimport ChannelParameter
from wdBuffers cimport WdBuffer
from wdDask cimport WdDask

include "wdChannels.pxi"


cdef class WdChannel(object):
    cdef:
        WdDask _card
        int _channel
        ADRange _adRange
        ChannelParameter _impedance
        ChannelParameter _adc_dither
        ChannelParameter _ai_coupling
        ChannelParameter _adc_bandwidth
        ChannelParameter _signal_fir
        dict _parameters
        WdBuffer _buffer

cdef class WdChannels(object):
    cdef:
        WdDask _card
        dict _channels
        ChannelParameter _impedance
        ChannelParameter _adc_dither
        ChannelParameter _ai_coupling
        ChannelParameter _adc_bandwidth
        ChannelParameter _signal_fir
        dict _parameters
        WdBuffer _buffer
