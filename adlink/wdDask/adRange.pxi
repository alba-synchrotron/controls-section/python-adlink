# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


cdef extern from "wd-dask.h":
    cdef int AD_B_10_V
    cdef int AD_B_5_V
    cdef int AD_B_2_5_V
    cdef int AD_B_1_25_V
    cdef int AD_B_0_625_V
    cdef int AD_B_0_3125_V
    cdef int AD_B_0_5_V
    cdef int AD_B_0_05_V
    cdef int AD_B_0_005_V
    cdef int AD_B_1_V
    cdef int AD_B_0_1_V
    cdef int AD_B_0_01_V
    cdef int AD_B_0_001_V
    cdef int AD_U_20_V
    cdef int AD_U_10_V
    cdef int AD_U_5_V
    cdef int AD_U_2_5_V
    cdef int AD_U_1_25_V
    cdef int AD_U_1_V
    cdef int AD_U_0_1_V
    cdef int AD_U_0_01_V
    cdef int AD_U_0_001_V
    cdef int AD_B_2_V
    cdef int AD_B_0_25_V
    cdef int AD_B_0_2_V
    cdef int AD_U_4_V
    cdef int AD_U_2_V
    cdef int AD_U_0_5_V
    cdef int AD_U_0_4_V
    cdef int AD_B_1_5_V
    cdef int AD_B_0_2145_V

ad_B_10_V     = AD_B_10_V
ad_B_5_V      = AD_B_5_V
ad_B_2_5_V    = AD_B_2_5_V
ad_B_1_25_V   = AD_B_1_25_V
ad_B_0_625_V  = AD_B_0_625_V
ad_B_0_3125_V = AD_B_0_3125_V
ad_B_0_5_V    = AD_B_0_5_V
ad_B_0_05_V   = AD_B_0_05_V
ad_B_0_005_V  = AD_B_0_005_V
ad_B_1_V      = AD_B_1_V
ad_B_0_1_V    = AD_B_0_1_V
ad_B_0_01_V   = AD_B_0_01_V
ad_B_0_001_V  = AD_B_0_001_V
ad_U_20_V     = AD_U_20_V
ad_U_10_V     = AD_U_10_V
ad_U_5_V      = AD_U_5_V
ad_U_2_5_V    = AD_U_2_5_V
ad_U_1_25_V   = AD_U_1_25_V
ad_U_1_V      = AD_U_1_V
ad_U_0_1_V    = AD_U_0_1_V
ad_U_0_01_V   = AD_U_0_01_V
ad_U_0_001_V  = AD_U_0_001_V
ad_B_2_V      = AD_B_2_V
ad_B_0_25_V   = AD_B_0_25_V
ad_B_0_2_V    = AD_B_0_2_V
ad_U_4_V      = AD_U_4_V
ad_U_2_V      = AD_U_2_V
ad_U_0_5_V    = AD_U_0_5_V
ad_U_0_4_V    = AD_U_0_4_V
ad_B_1_5_V    = AD_B_1_5_V
ad_B_0_2145_V = AD_B_0_2145_V
