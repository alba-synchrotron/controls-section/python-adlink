# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


cdef extern from "basic_type.h":
    ctypedef unsigned char U8
    ctypedef short I16
    ctypedef unsigned short U16
    ctypedef long I32
    ctypedef unsigned long U32
    ctypedef signed long long int I64
    ctypedef unsigned long long int U64
    ctypedef float F32
    ctypedef double F64

    ctypedef enum BOOLEAN:
        FALSE = 0
        TRUE = 1

    cdef U8 FIRSTBYTE(VALUE)
    cdef U8 SECONDBYTE(VALUE)
    cdef U8 THIRDBYTE(VALUE)
    cdef U8 FOURTHBYTE(VALUE)
