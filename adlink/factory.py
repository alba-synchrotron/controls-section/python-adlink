# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

from adlink.utils.singleton import __singleton__
from adlink.utils.pcicard import PCICard

# TODO: improve this submodule availability
try:
    from adlink.wdDask import wdDask
except:
    wdDask = None
try:
    from adlink.d2kDask import d2kDask
except:
    d2kDask = None

from hwinfo.pci import PCIDevice
from hwinfo.pci.lspci import LspciNNMMParser
from subprocess import check_output

@__singleton__
class Factory(object):
    """
This singleton object is the basic element of this library to explore your
computer to find Adlink cards and report the user to, then, provide information
and control.

>>> import adlink; factory = adlink.Factory()
>>> factory.nCards
 2
>>> factory.nCardsByModel
 {'9852': 1, 'a005': 1}

The available modules will report the user which of those card can be accessed:
>>> factory.submodules
 ['wd-dask']
>>> factory.getInfo('a005', 0).cardModule
 'd2kDask'
As this module is not in the list of available submodules, it is not possible
to get a card object to control it.

But from cards controlled by a available submodule, one can request the card
object:
>>> card = factory.getCard('9852', 0)
    """
    __pciDevices = {}
    __nPCIcards = 0
    __nCardsByModel = {}

    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.__searchPCI()

    def __searchPCI(self):
        # From the https://github.com/rdobson/python-hwinfo README
        # Obtain the output of lspci -nnmm from somewhere
        lspci_output = check_output(["lspci", "-nnmm"])
        # Parse the output using the LspciNNMMParser object
        parser = LspciNNMMParser(lspci_output)
        device_recs = parser.parse_items()
        # Instantiate the records as PCI devices
        pci_devs = [PCIDevice(device_rec) for device_rec in device_recs]
        # Use the PCIDevice class to query info for subdevices
        for i, dev in enumerate(pci_devs):
            if dev.is_subdevice():
                if dev.get_vendor_name() in ['Adlink Technology']:
                    self.__appendNewPciDevice(dev, i)

    def __appendNewPciDevice(self, dev, idx):
        if not dev.get_device_id() in self.__pciDevices:
            self.__pciDevices[dev.get_device_id()] = []
        if not dev.get_device_id() in self.__nCardsByModel:
            self.__nCardsByModel[dev.get_device_id()] = 0
        self.__pciDevices[dev.get_device_id()].append(PCICard(dev, idx))
        self.__nPCIcards += 1
        self.__nCardsByModel[dev.get_device_id()] += 1

    @property
    def nCards(self):
        """Return the total number of Adlink cards found in the system.
        """
        return self.__nPCIcards

    @property
    def cardModels(self):
        """Return a list of models found of Adlink cards in the system.
        """
        return list(self.__pciDevices.keys())

    @property
    def nCardsByModel(self):
        """Dictionary structure with the Adlink card models found in the system
           followed by how many of each has been found.
        """
        return self.__nCardsByModel

    def getInfo(self, model, idx):
        """For a given model and index pair give the user some hwinfo.
        """
        if not model in self.__pciDevices:
            raise KeyError("No %s cards present" % (model))
        if idx >= self.__nCardsByModel[model]:
            raise IndexError("No %s cards present with the requested index"
                             % (model))
        return self.__pciDevices[model][idx]

    def getCard(self, model, idx):
        """Get an object that represents (and infact interfaces) an specific
           card.
        """
        info = self.getInfo(model, idx)
        # TODO: based on the info.cardModule, build the submodule object
        #       if supported
        if info.cardModule in ['wdDask']:
            return wdDask.WdDask(info.cardType, idx)
        raise Exception("No module present for the requested card")

    @property
    def submodules(self):
        """List of submodules available to access different Adlink SDKs.
        """
        subm = []
        if wdDask is not None:
            subm.append('wd-dask')
        if d2kDask is not None:
            subm.append('d2k-dask')
        return subm
