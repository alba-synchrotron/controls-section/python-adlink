# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

from ..wdDask import cardType


_cardSubmodules = {'9852': ['wdDask', cardType._PCIe_9852],
                   'a005': ['d2kDask', None]}


# TODO: PCICard should be subclass of Card to support more than pci


class PCICard(object):

    __pciIndex = None
    __dev = None
    __cardModule = None
    __modelCode = None

    def __init__(self, dev, idx, *args, **kwargs):
        super(PCICard, self).__init__(*args, **kwargs)
        self.__dev = dev
        self.__pciIndex = idx
        # TODO: based on the card id, find if there is an adlink module for it.
        self.__cardModule, code = _cardSubmodules.get(self.id, [None, None])
        if code is not None:
            self.__modelCode = cardType.CardType(code)

    def __str__(self):
        return "PCICard(%s)" % (self.id)

    def __repr__(self):
        return self.__str__()

    @property
    def pci_index(self):
        """Cardinal number of the PCI position of the card.
        """
        return self.__pciIndex

    @property
    def bus_id(self):
        """PCI identification address of the card.
        """
        return self.__dev.get_device_bus_id()

    @property
    def klass(self):
        """PCI card class identification name.
        """
        return "%s:%s" % (self.__dev.get_pci_class(),
                          self.__dev.rec['pci_device_class_name'])

    @property
    def id(self):
        """Card identification code from the vendor (Adlink)..
        """
        return self.__dev.get_device_id()

    @property
    def name(self):
        """Card identification name from the vendor (Adlink)..
        """
        return self.__dev.get_device_name()

    @property
    def cardModule(self):
        """Submodule of this python-adlink module necessary to have access
           to the given card.
        """
        return self.__cardModule

    @property
    def cardType(self):
        """Card identification in this python-adlink module.
        """
        return self.__modelCode
