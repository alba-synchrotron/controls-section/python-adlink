# -*- coding: utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"


import os
from setuptools import setup, find_packages, Command

try:
    from Cython.Build import cythonize
except ImportError:
    raise ImportError("Having Cython installed is required")
# IMPORTANT: the setuptools import must be before the Cython one
#            if not, the cythonize() output will not be a:
#            <setuptools.extension.Extension object at 0x...> but a:
#            <distutils.extension.Extension object at 0x...>
#            that will not be accepted by the setup().


projectName = 'adlink'
shortDescription = "Python module to provide access to Adlink cards "\
                   "(using their privative drivers)."
# we use semantic versioning (http://semver.org/) and we update it using the
# bumpversion script (https://github.com/peritus/bumpversion)
__version__ = '0.1.2-alpha'

longDescription = '''
This code has been written to provide Python access to Adlink cards, managing
all the peculiarities of the lower level library and kernel driver using
Cython.

It is necessary to have a recent enough version of Cython installed
(development made using 0.25.2) as well as the headers, library (.so) and
kernel modules (.ko), the sources of are subject to a NDA.
'''

classifiers = []
classifiers.append('Development Status :: 1 - Planning')
classifiers.append('Intended Audience :: Developers')
classifiers.append('Intended Audience :: Science/Research')
classifiers.append('License :: OSI Approved :: '
                   'GNU General Public License v3 or later (GPLv3+)')
classifiers.append('Operating System :: POSIX')
classifiers.append('Programming Language :: Cython')
classifiers.append('Topic :: Scientific/Engineering :: '
                   'Interface Engine/Protocol Translator')
classifiers.append('Topic :: Software Development :: Embedded Systems')
classifiers.append('Topic :: Software Development :: Libraries :: '
                   'Python Modules')
classifiers.append('Topic :: System :: Hardware')
# for the classifiers review see:
# https://pypi.python.org/pypi?%3Aaction=list_classifiers
#     Development Status :: 1 - Planning
#     Development Status :: 2 - Pre-Alpha
#     Development Status :: 3 - Alpha
#     Development Status :: 4 - Beta
#     Development Status :: 5 - Production/Stable


def find_file_by_extension(path='.', ext='.pyx'):
    files = []
    for root, dirs, filenames in os.walk(path):
        for fname in filenames:
            if fname.endswith(ext):
                files.append(os.path.join(root, fname))
    return files


def find_directories(path='.', names=None):
    directories = []
    if names is not None and isinstance(names, list):
        for root, dirs, filenames in os.walk(path):
            if len(dirs) > 0 and dirs[0] in names:
                dname = "%s/%s" % (root, dirs[0])
                if not dname in directories:
                    directories.append(dname)
    return directories


def find_pyx(path='.'):
    return find_file_by_extension(path, ext='.pyx')


def find_so(path='.'):
    return find_file_by_extension(path, ext='.so')


submodules = {'wdDask': {'include': ['/usr/local/include/wd-dask'],
                         'lib': ['wd_dask64']
                         }
              }


def isSubmodule(fname):
    lname = fname.split('/', 2)
    if lname[0] != '.':
        raise AssertionError("file name not understood")
    if lname[1] != projectName:
        raise AssertionError("file name not under the many module")
    if lname[2].count('/'):
        return True
    return False


def getSubmoduleName(fname):
    return fname.split('/', 2)[2].rsplit('/', 1)[0]


def areIncludesAvailable(submodule):
    if not submodule in submodules:
        return False
    if not 'includesAvailable' in submodules[submodule]:
        answer = []
        for include in submodules[submodule]['include']:
            answer.append(os.path.isdir(include))
        submodules[submodule]['includesAvailable'] = all(answer)
    return submodules[submodule]['includesAvailable']


def appendInclude(source, submodule):
    if submodule in submodules and 'include' in submodules[submodule]:
        source.include_dirs += submodules[submodule]['include']

def appendLibrary(source, submodule):
    if submodule in submodules and 'lib' in submodules[submodule]:
        source.libraries += submodules[submodule]['lib']


# FIXME: it is call in all commands, even when 'clean'
def build_extensions():
    """
        This method can be simply:
        return cythonize(find_pyx(), language_level=3)
        But, this way we will be able to add libraries and includes for each
        of the files if need be.
    """
    extensions = []
    files = find_pyx()
    for file in files:
        # print(">> file: %s" % (file))
        sources = cythonize(file)
        if isSubmodule(file):
            subm = getSubmoduleName(file)
            # print(">> submodule: %s"% (subm))
            if areIncludesAvailable(subm):
                for source in sources:  # TODO: improve. many failure points
                    # print(">> source: %s" % (source.name))
                    appendInclude(source, subm)
                    appendLibrary(source, subm)
                    # print(">> includes: %s" % (source.include_dirs))
                    # print(">> libraries: %s" % (source.libraries))
        extensions += sources
    return extensions


# cmdclass = {}
# ext_modules = []
# projectDir = projectName
# 
# extensions = [
#     # {'name': '%s.utils' % (projectName),
#     #  'dir': 'adlink/utils',
#     #  'src': ['__init__.pyx']},
#     {'name': '%s' % (projectName),
#      'dir': '%s' % (projectDir),
#      #'src': ['__init__.pyx'],
#      'include': ['.']},
#     {'name': '%s.wdDask' % (projectName),
#      'dir': '%s/wdDask' % (projectDir),
#      #'src': ['__init__.pyx'],
#      'include': ['.', '/usr/local/include/wd-dask'],
#      'lib': ['wd_dask64']},]
# #packages = []
# packagesDir = {}
# for i in range(len(extensions)):
#     #packages.append(extensions[i]['name'])
#     packagesDir[extensions[i]['name']] = extensions[i]['dir']
# 
# 
# def find_pyx(path='.', gext=None):
#     '''Search in a directory for the pyx files to return them in a list.
#        If the gext (generated extension) is set, the returned list is the files
#        cythonized.
#     '''
#     pyx_files = []
#     for root, dirs, filenames in os.walk(path):
#         for fname in filenames:
#             if fname.endswith('.pyx'):
#                 if gext is not None:
#                     gname = fname.replace('.pyx', gext)
#                     # TODO: check if exist
#                     pyx_files.append(os.path.join(root, gname))
#                 else:
#                     pyx_files.append(os.path.join(root, fname))
#     return pyx_files
# 
# 
# def prepareExtensions():
#     global ext_modules
#     sources = []
#     for extension in extensions:
#         print(">> Preparing Extension %s" % (extension['name']))
#         if use_cython:
#             files = cythonize(find_pyx(extension['dir']))
#         else:
#             files = find_pyx(extension['dir'], gext='.c')
#         #files = []
#         #for fileName in extension['src']:
#         #    files.append("%s/%s" % (extension['dir'], fileName))
#         #print(">>\tsource:%s" % (files))
#         #if use_cython:
#         #    source = cythonize(files)
#         #    print(">>\tcythonized:%s" % ([s.sources[0] for s in source]))
#         #else:
#         #    source = [fileName.replace('.pyx', '.c') for fileName in files]
#         include = extension.get('include', [])
#         lib = extension.get('lib', [])
#         print(">>\tincludes:%s\n>>\tlib:%s\n" % (include, lib))
#         extensionObj = Extension(extension['name'],
#                                  include_dirs=include,
#                                  libraries=lib,
#                                  sources=files,
#                                  language="c",
#                                  extra_compile_args=["-static", "-fPIC"])
#         ext_modules += [extensionObj]
#     cmdclass.update({'build_ext': build_ext})
# 
# #     if use_cython:
# #         sources = []
# #         for extension in extensions:
# #             print(">> use_cython: Extension %s" % (extension['name']))
# #             files = []
# #             for file in extension['src']:
# #                 files.append("%s/%s" % (extension['dir'], file))
# #             print(">> \tsource:%s" % (files))
# #             source = cythonize(files)
# #             include = extension.get('include', [])
# #             lib = extension.get('lib', [])
# #             print(">> \tcythonized:%s" % ([s.sources for s in source]))
# #             print(">>\tincludes:%s\n>>\tlib:%s\n" % (include, lib))
# #             extensionObj = Extension(extension['name'],
# #                                      include_dirs=include,
# #                                      libraries=lib,
# #                                      sources=source[0].sources,
# #                                      language="c",
# #                                      extra_compile_args=["-static", "-fPIC"])
# #             ext_modules += [extensionObj]
# #             sources += source
# #         cmdclass.update({'build_ext': build_ext})
# # 
# #         class sdist(_sdist):
# #             def run(self):
# #                 from Cython.Build import cythonize
# #                 cythonize(sources)
# #                 _sdist.run(self)
# #         cmdclass.update({'sdist': sdist})
# #     else:
# #         for extension in extensions:
# #             source = [file.replace('.pyx', '.c') for file in extension['src']]
# #             include = extension.get('include', [])
# #             lib = extension.get('lib', [])
# #             extensionObj = Extension(extension['name'],
# #                                      include_dirs=include,
# #                                      libraries=lib,
# #                                      sources=source,
# #                                      language="c",
# #                                      extra_compile_args=["-static", "-fPIC"])
# #             ext_modules += [extensionObj]
# #             print(">> NOT use_cython: Extension %s" % extensionObj.name)


class CleanCommand(Command):
    """Custom clean command to tidy up the project root."""
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        sources = ['./build', './*.egg-info']
        files = []
        for fileName in find_pyx():
            files.append(fileName.replace('.pyx', '.c'))
            files.append(fileName.replace('.pyx', '.cpp'))
        for soFile in find_so():
            files.append(soFile)
        for pyc in find_file_by_extension(ext='.pyc'):
            files.append(pyc)
        for file in files:
            os.system('rm -vrf %s' % file)
        for directory in find_directories(names=['__pycache__']):
            os.system('rm -vrf %s' % directory)


cmdclass = {}
cmdclass.update({'clean': CleanCommand})


setup(name=projectName,
      version=__version__,
      license=__license__,
      description=shortDescription,
      long_description=longDescription,
      author=__author__,
      author_email=__email__,
      setup_requires=['cython', 'python-hwinfo'],
      ext_modules=build_extensions(),
      cmdclass=cmdclass,
      packages=find_packages(),
      classifiers=classifiers
      )
